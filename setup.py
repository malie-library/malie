#!/usr/bin/env python
"""malie installation script."""

import setuptools
import setuptools.command.build_py
import setuptools.command.develop

# Borrowed from https://jichu4n.com/posts/how-to-add-custom-build-steps-and-commands-to-setuppy/
class BuildPyCommand(setuptools.command.build_py.build_py):
    """Invoke the protobuf-setuptools build hook"""
    def run(self):
        self.run_command('build_proto')
        super().run()

class DevelopCommand(setuptools.command.develop.develop):
    """Invoke the protobuf-setuptools hook for develop installs, too"""
    def run(self):
        self.run_command('build_proto')
        super().run()

def main():
    """malie installation wrapper"""
    kwargs = {
        'name': 'malie',
        'use_scm_version': True,
        'author': 'nago',
        'author_email': 'nago@malie.io',
        'description': 'malie is a Pokemon TCG Database toolset',
        'url': 'https://gitlab.com/malie-library/malie',
        'packages': setuptools.find_namespace_packages(include=['malie.*']),
        'classifiers': [
            "Development Status :: 2 - Pre-Alpha",
            "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
            "Natural Language :: English",
            "Operating System :: OS Independent",
            "Programming Language :: Python :: 3",
        ],
        'cmdclass': {
            'build_py': BuildPyCommand,
            'develop': DevelopCommand,
        },
        'include_package_data': True,
        'entry_points': {
            'console_scripts': [
                'alola = malie.scrapers.ptcgo.alola:main',
                'kecleon = malie.scrapers.ptcgo.kecleon:main',
                'metamon = malie.scrapers.ptcgo.metamon:shim',
                'smeargle = malie.scrapers.ptcgo.smeargle:main',
                'pkcrop = malie.scrapers.ptcgo.crop:main',
            ]
        },
        'setup_requires': [
            'protobuf-setuptools',
            'setuptools_scm',
        ],
        'install_requires': [
            'lxml',
            'requests',
            'dateparser',
            'xmltodict',
            'unitypack >= 0.9.0',
            'jsonschema',
            'langcodes',
            'pyxdg',
            'b2sdk',
            'discord.py',
            'protobuf',
            'netfleece >= 0.1.1',
            'netfile >= 0.1.3',
            'python-editor',
            'setuptools_scm',
        ],
        'python_requires': '>=3.7',
    }

    with open("README.rst", "r") as fh:
        kwargs['long_description'] = fh.read()

    setuptools.setup(**kwargs)

if __name__ == '__main__':
    main()
