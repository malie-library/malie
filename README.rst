Malie Library - Core Toolset
============================

Alola!

This is the Malie Library reference section’s core toolset. ``malie`` is
a set of data gathering and manipulation programs for Pokémon: TCG card
and asset data.

What’s in here?
---------------

The malie toolset contains several scrapers that can acquire data from
several official sources:

-  ``ptcgo`` - Pokémon: Trading Card Game Online
-  ``carddex`` - Pokémon Trading Card Game Card Dex
-  ``tpci`` - The Pokémon Company International website
   (https://www.pokemon.com)
-  ``kkp`` - The Pokémon Company (株式会社ポケモンー ‘Kabushiki Kaisha
   Pokemon’) website (https://www.pokemon-card.com/)

It also contains one additional scraper to scrape an unofficial source:

-  ``pkmncards`` - PkmnCards.com, by Adam Capriola and the PkmnCards
   curator team.

However, only the ``ptcgo`` scraper is fully operational right
now. Please pardon the mess in the others for now.

What is this used for?
----------------------

The Malie toolset is used to acquire data from several sources and
convert them all into a normalized, compatible format. Malie then
correlates the card definitions and metadata from multiple sources into
a converged format. It uses all available sources to reduce any
inconsistencies or errors present in the source data.

That's the plan, anyway. Right now, ``ptcgo`` only saves to a
domain-specific format.

Why publish this?
-----------------

I wanted malie.io to be powered by open source, and principally from
primary, official sources.

By publishing my toolset, my hope is that the Pokemon: TCG community at
large will be able to use these tools to improve and enrich their own
sites, reduce errors, and increase cross-site collaboration to develop
new open-source standards.

This project is my love letter to Pokemon, the TCG, and free open source
software.

Project Status & Roadmap
------------------------

This repository is in a very alpha state, and not everything is
integrated well yet. Only the PTCGO tools are polished and should
work well. Consider the others merely pre-alpha drafts.

- ``ptcgo`` Functional beta. Card definitions, localizations, and
  assets are supported. Card data saves to a domain-specific JSON
  format. Updates to card and translation metadata are
  supported. Updates to card assets are presently ignored.

- ``carddex`` Alpha. Card definitions can be downloaded to a
  domain-specific format in six languages. Card assets are not
  downloaded. No UI or CLI tools are formalized.

- ``tpci`` Pre-alpha. Only extension release information is working.

- ``kkp`` Draft only. Completely non-working.

- ``pkmncards`` Draft only. Completely non-working.

The domain-specific format for ``ptcgo`` is well-established and will
form a preliminary basis for developing a normalized storage format
between all scrapers. The next major efforts will be ``tpci`` and
``carddex``, as time permits.

Requirements & Dependencies
---------------------------

To install or use the tools, you'll need some system dependencies
for building the python packages:

- python >= 3.7
- python3-devel
- gcc-c++
- libjpeg
- protobuf-compiler
- libxml
- libxslt

The tools have some additional runtime dependencies:

- lynx, for viewing patchnotes in the terminal.
- xdelta3, for patching client files.
- ImageMagick, for cropping card assets. Eventually, I plan to replace this with native PIL calls.
- oxipng, for compressing card assets.

Installation
------------

Once you have the build dependencies, there are several ways to
install, depending on your preferences.

1. Obtain sources:
  - Latest development version from git:
    - ``git clone https://gitlab.com/malie-library/malie.git``
  - Latest release version from git:
    - ``git clone --branch v0.0.0 https://gitlab.com/malie-library/malie.git``

2. Enter the source directory:
   - ``pushd malie``

3. Install to virtual environment or user environment:
   - If you are not using a virtual environment, you can just:
     - ``make userinstall``
   - If you would like to use pipenv, you can:
     - ``pipenv sync``
     - ``pipenv shell``
     - ``make install``
   - If you'd like to use a virtual environment of your choosing, create and/or activate it, and then:
     - ``make install``

The ``install``, ``userinstall``, ``develop``, and ``userdevelop``
makefile targets use pip to make sure that Malie's fork of UnityPack
is installed. You can, if you prefer, use ``pip3`` or ``python3
setup.py <install|develop>`` directly, but you'll have to install the
prerequisite yourself:
  - ``pip3 install [--user] git+https://gitlab.com/malie-library/unitypack.git@0.9.1a0#egg=unitypack``

Getting Cooking (PTCGO)
-----------------------

- You can run ``alola`` as a "hello world" script to test that the
  dependencies are installed correctly and get information on the
  various directories Malie will use for configuration, cache data,
  and output data.

- ``kecleon`` is the settings manager and client updater module. It
  checks for patches to the game and will attempt to download and
  install them. It will then attempt to find updates to the asset
  manifest. It tries its hardest to look and act exactly like the
  official patcher.

  You can run ``kecleon --edit`` to open its config file in your
  $EDITOR. The 'static' and 'dynamic' sections are debug-only and can
  be ignored.

  You can check for new client versions and asset manifests by simply
  running ``kecleon``.

- ``metamon`` will connect to https://malie.io and download new card
  definitions, localizations, and other information. If the ``ditto``
  extension is present, it will use that instead to connect directly
  to the game servers to download the same information. The source for
  ditto is not available to prevent abuse.

- ``smeargle`` is the art extractor. ``smeargle --help`` will show
  common options, and ``smeargle <command> --help`` will show options
  for subcommands. Check the directories it will use with ``alola``,
  because it is capable of downloading and generating large amounts of
  data.
