#!/usr/bin/python3
"""Provides the DLO class, the \"Dict-like Object.\""""

from collections import Mapping

class DLO(dict):
    """Dict-like Object. Once a mapping is established, it cannot be
    overwritten, unless explicitly deleted first."""

    def __setitem__(self, k, v):
        if k in self:
            if self[k] != v:
                raise Exception("DLO key '%s' cannot take two values:"
                                " %s --> %s" % (k, self[k], v))
        elif k is not None:
            super().__setitem__(k, v)
            return True
        return False

    def overwrite(self, k, v):
        if k in self:
            del self[k]
        self[k] = v

    def update(self, other=None, **kwargs):
        # https://stackoverflow.com/questions/30241688/
        updated = False
        if other is not None:
            for k, v in other.items() if isinstance(other, Mapping) else other:
                updated |= self.__setitem__(k, v)
        for k, v in kwargs.items():
            updated |= self.__setitem__(k, v)
        return updated

    def rsetitem(self, k, v):
        """Recursively set a value; if the value exists and is itself a DLO,
        recursively update that value instead of trying to overwrite it."""
        if not k in self:
            return self.__setitem__(k, v)
        elif isinstance(self[k], DLO):
            return self[k].rupdate(v)
        return self.__setitem__(k, v)

    def rupdate(self, other=None, **kwargs):
        """Recursively update the dict instead of testing for equality on nested
        values. Normally, DLO will object to sub-DLO values being updated to a
        new superset, but rupdate will tolerate this."""
        updated = False
        if other is not None:
            for k, v in other.items() if isinstance(other, Mapping) else other:
                updated |= self.rsetitem(k, v)
        for k, v in kwargs.items():
            updated |= self.rsetitem(k, v)
        return updated
