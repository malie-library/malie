"""
jsondiff provides various utilities for diffing json objects.
"""

from collections import namedtuple
from collections.abc import Iterable
import inspect
import itertools
import json
import sys

def atomic(obj):
    """
    atomic checks to see if this object is an atom.

    Strings, while iterable, are specially considered atoms.
    Otherwise, atoms are any non-iterable object.
    """
    return (isinstance(obj, str) or not
            isinstance(obj, Iterable))

def object_like(obj):
    """object_like checks to see if this object behaves like a json object."""
    return hasattr(obj, 'items')

def array_like(obj):
    """listlike checks to see if this object behaves like a json array."""
    # Note: If it's not atomic, it's iterable. If it's not object-like, it's a linear collection.
    return (not atomic(obj)) and (not object_like(obj))

def from_file(filename):
    with open(filename, 'r') as handle:
        return json.load(handle)

def list_compare(a, b):
    """Compare two lists, with position-sensitive comparisons."""
    left = type(a)()
    right = type(b)()
    for lval, rval in itertools.zip_longest(a, b):
        lval, rval = json_compare(lval, rval, True)
        left.append(lval)
        right.append(rval)
    return left, right

def list_compare_unordered(a, b):
    """Compare two lists, with position independent comparisons."""
    ltmp = type(a)()
    rtmp = type(b)()
    left = type(a)()
    right = type(b)()
    for lval in a:
        if lval not in b:
            ltmp.append(lval)
    for rval in b:
        if rval not in a:
            rtmp.append(rval)
    for lval, rval in itertools.zip_longest(ltmp, rtmp):
        lval, rval = json_compare(lval, rval, False)
        if lval is not None:
            left.append(lval)
        if rval is not None:
            right.append(rval)
    return left, right

def json_compare(a, b, ordered=True):
    """
    Compares two json-like objects recursively and returns the diffs.

    Returns two json-like objects, which are copies of `a` and `b`, respectively
    with all common elements between them removed.
    For lists of unequal length, None will be used as the empty value.

    if a or b are not both lists or dicts, a and b are returned as-is.
    """
    if isinstance(a, dict) and isinstance(b, dict):
        left = type(a)()
        right = type(b)()
        for k in a.keys() - b.keys():
            left[k] = a[k]
        for k in b.keys() - a.keys():
            right[k] = b[k]
        for k in a.keys() & b.keys():
            if a[k] != b[k]:
                left[k], right[k] = json_compare(a[k], b[k], ordered)
        return left, right
    if isinstance(a, list) and isinstance(b, list):
        if ordered:
            return list_compare(a, b)
        return list_compare_unordered(a, b)
    return a, b

def to_dot(jobj, path=None):
    """
    to_dot takes a json-like object and returns it as a list of k,v pairs.

    The keys are in a json dot notation (e.g. field.subdict.entries[5].property),
    and the values are returned as-is (i.e. not filtered through str or repr.)
    """
    path = path or []
    entries = []
    if isinstance(jobj, list):
        # Lists append with e.g. parent[0] instead of parent.[0]
        prefix = path.pop() if path else ""
        for k, v in enumerate(jobj):
            node = "{:s}[{:d}]".format(prefix, k)
            entries.extend(to_dot(v, path + [node]))
    elif isinstance(jobj, dict):
        for k, v in jobj.items():
            node = "{:s}".format(str(k))
            entries.extend(to_dot(v, path + [node]))
    else:
        entries.append((".".join(path), jobj))
    return entries

def json_callback(callback_fn, key, val):
    if len(inspect.signature(callback_fn).parameters) >= 2:
        return callback_fn(key, val)
    return callback_fn(val)

def rmap(leaf_fn, json_obj):
    """
    rmap recurisvely maps a JSON-like object similarly to the map() builtin.

    Given a a leaf(v) or a leaf(k, v) function, recursively map a JSON object's
    leaf nodes, v, to leaf_fn([k,] v), where k is either the dictionary key OR
    a sequence ordinal position describing where that value is in the json
    array.

    A leaf function of leaf(v) will not be called with any key values.

    is json_obj is an atomic value at the top level, k will be None.
    """
    def _interior_fn(key, val):
        if atomic(val):
            return json_callback(leaf_fn, key, val)
        return rmap(leaf_fn, val)

    # Object-like:
    if object_like(json_obj):
        return type(json_obj)({key: _interior_fn(key, val) for
                               key, val in json_obj.items()})

    # String, number, or other atomic object:
    if atomic(json_obj):
        return _interior_fn(None, json_obj)

    # Array-like:
    return type(json_obj)([_interior_fn(pos, val) for
                           pos, val in enumerate(json_obj)])


def rfilter(filter_fn, json_obj, keymap=None):
    """
    rfilter recursively filters an object, returning a subset of the original object.

    each leaf value is passed to filter_fn(key, value). If the function returns true,
    the leaf is kept in the tree. If keymap is provided, keys are changed to whatever
    is returned by keymap(key).

    if filter_fn takes only one argument, it will be called as filter_fn(value).

    The key given for items in an array will be its ordinal position.
    Top-level, atomic items will pass along a key of None.
    """

    def _interior_fn(key, val):
        if atomic(val):
            return val if json_callback(filter_fn, key, val) else None
        return rfilter(filter_fn, val, keymap)

    # Objects:
    if object_like(json_obj):
        tmp = {}
        for key, val in json_obj.items():
            res = _interior_fn(key, val)
            if res:
                key = keymap(key) if keymap else key
                tmp[key] = res
        return type(json_obj)(tmp)

    # Something atomic:
    if atomic(json_obj):
        tmp = filter_fn(None, json_obj)
        return tmp if tmp else type(json_obj)()

    # Arrays:
    tmp = []
    for pos, val in enumerate(json_obj):
        res = _interior_fn(pos, val)
        if res:
            tmp.append(res)
    return type(json_obj)(tmp)


def rsubset_array(obj1, obj2, pia=False):
    # When you go to Computer Science school, they teach you to use
    # phrases like "Comutual Recursion." Unfortunately, nobody teaches
    # you how to code.

    if len(obj1) > len(obj2):
        return False

    if not pia:
        for left, right in zip(obj1, obj2):
            if not rsubset(left, right, pia):
                return False
        return True

    tmp = list(obj2)
    for value in obj1:
        remove = False
        for other in tmp:
            if rsubset(value, other, pia):
                remove = True
                # Note: Greedy, might not find optimal matches.
                break
            if remove:
                tmp.remove(value)
            else:
                return False
    return True

def rsubset(obj1, obj2, pia=False):
    # Object-like values
    if object_like(obj1) and object_like(obj2):
        for k, v in obj1.items():
            if not k in obj2 or rsubset(v, obj2[k], pia):
                return False
        return True

    # Array-like values (position-aware)
    if array_like(obj1) and array_like(obj2):
        return rsubset_array(obj1, obj2, pia)

    # Everything else:
    return obj1 == obj2


def pprint(json_obj, indent=2):
    """pprint pretty-prints a json-like object."""
    print(json.dumps(json_obj, indent=indent))


class JsonObject:
    def __init__(self, json_obj):
        self.json_obj = json_obj

    def __len__(self):
        return len(self.json_obj)

    def __getitem__(self, key):
        return self.json_obj[key]

    def __setitem__(self, key, value):
        self.json_obj[key] = value

    def __delitem__(self, key):
        del self.json_obj[key]

    def __iter__(self):
        return iter(self.json_obj)

    def __reversed__(self):
        return reversed(self.json_obj)

    def __contains__(self, key):
        return key in self.json_obj

    @staticmethod
    def load(*args, **kwargs):
        return JsonObject(json.load(*args, **kwargs))

    @staticmethod
    def loads(*args, **kwargs):
        return JsonObject(json.loads(*args, **kwargs))

    def pprint(self, indent=2):
        pprint(self.json_obj, indent=indent)

    def rmap(self, leaf_fn):
        return rmap(leaf_fn, self.json_obj)

    def rfilter(self, filter_fn, keymap=None):
        return rfilter(filter_fn, self.json_obj, keymap)

    def dot(self, path=None):
        return to_dot(self.json_obj, path)

    def compare(self, other, ordered=True):
        if hasattr(other, 'json_obj'):
            against = other.json_obj
        else:
            against = other
        return json_compare(self.json_obj, against, ordered)

    def dot_compare(self, other, ordered=True):
        left, right = self.compare(other, ordered)
        return DotDiff.from_json(left, right)


Change = namedtuple('Change', ['path', 'symbol', 'value'])


class DotDiff:
    """DotDiff is a dot-syntax diffstat of two json-like objects."""
    def __init__(self, changes: [Change]):
        self.changes = changes

    def __str__(self):
        lines = []
        for change in self.changes:
            lines.append(f"{change.symbol}{change.path} = {change.value}")
        return "\n".join(lines)

    @staticmethod
    def from_json(old, new):
        """
        from_json takes two json diff objects and creates a dot-syntax summary.
        """
        changes = []
        for k, v in to_dot(old):
            changes.append(Change(k, "-", repr(v)))
        for k, v in to_dot(new):
            changes.append(Change(k, "+", repr(v)))
        tmp = DotDiff(changes)
        return tmp.sort()

    def print(self, indent=1, file=sys.stdout):
        for change in self.changes:
            print("{:{:d}s}{}{} = {}".format("", indent, change.symbol,
                                             change.path, change.value),
                  file=file)

    def to_list(self):
        return ["{}{} = {}".format(change.symbol, change.path, change.value)
                for change in self.changes]

    def sort(self):
        self.changes = sorted(self.changes, key=lambda c: (c.path, -ord(c.symbol)))
        return self
