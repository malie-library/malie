#!/usr/bin/env python3

import time
import logging
from lxml import html, etree
import requests

class Forbidden403(RuntimeError):
    pass

class WebElement(html.HtmlElement):
    def _one(self, res):
        assert len(res) < 2
        if res:
            return res[0]
        return None

    def xpath1(self, *args, **kwargs):
        res = self.xpath(*args, **kwargs)
        return self._one(res)

    def find_class1(self, *args, **kwargs):
        res = self.find_class(*args, **kwargs)
        return self._one(res)

    def xclass(self, classname, tag='div'):
        xquery = f".//{tag}[contains(concat(' ', normalize-space(@class), ' '), ' {classname} ')]"
        return self.xpath(xquery)

class WebScraper:
    def __init__(self, retries=5, delay=30):
        self.retries = retries
        self.delay = delay
        self.headers = {}

    def fetch(self, url):
        retries = self.retries
        while retries:
            logging.info("fetching URL: %s", url)
            page = requests.get(url, headers=self.headers)
            if page.status_code == 200:
                return page
            elif page.status_code == 403:
                logging.error("403 Forbidden: %s", url)
                raise Forbidden403("403 Forbidden: %s" % url)
            logging.warning("Tried to fetch '%s'; got status %d;", url, page.status_code)
            retries -= 1
            if retries:
                logging.warning("%d retries left, retrying in %ds ...", retries, self.delay)
                time.sleep(self.delay)
        raise RuntimeError(f"Could not fetch {url} after {self.retries} tries")

    def fetch_tree(self, url, elem=None):
        elem = elem or WebElement

        parser = etree.HTMLParser()
        parser_lookup = etree.ElementDefaultClassLookup(element=elem)
        parser.set_element_class_lookup(parser_lookup)

        page = self.fetch(url)
        return html.fromstring(page.content, parser=parser)

    def check_url(self, url):
        page = requests.head(url, headers=self.headers)
        return page.status_code == 200
