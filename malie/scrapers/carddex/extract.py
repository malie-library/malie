#!/usr/bin/env python3

import argparse
import io
import json
import logging

import bindata
import carddex
import normalize
import xmldata

def write_data(data, filename):
    if isinstance(data, bytes):
        mode = "wb+"
    else:
        mode = "w+"
    with open(filename, mode) as fp:
        num = fp.write(data)
    logging.info("Wrote %d bytes to %s", num, filename)

def bin_to_standard(data: bytes) -> dict:
    stream = io.BytesIO(data)
    jdata = bindata.parse_stream(stream)
    return bindata.standardize(jdata)

def xml_to_standard(data: str) -> dict:
    jdata = xmldata.parse(data)
    return xmldata.standardize(jdata)

def main():
    desc = "Extract CardDex databases from UnityFS Asset Bundles"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("--normalize",
                        action='store_true',
                        help="Normalize JSON output to a common format.")
    parser.add_argument("--intermediate",
                        action='store_true',
                        help=
                        """
                        Write out the intermediate database file.
                        For v1.0 files, this will write out the raw,
                        unprocessed XML as {file}.xml.

                        For v1.2+ files, this will write out the raw,
                        unprocessed binary database file as {file}.bin.
                        """)
    parser.add_argument("filename",
                        type=str,
                        help=
                        """
                        Filename of the Unity bundle to extract a card
                        definitions database from.
                        """)
    args = parser.parse_args()

    jdata = None
    ext = None

    fh = open(args.filename, "rb")

    lang = 'en'
    for code in {'en', 'es', 'fr', 'it', 'de', 'ptbr'}:
        if f"_{code}" in args.filename:
            lang = code

    data = carddex.extract_rawdb(fh)
    if isinstance(data, str):
        jdata = xml_to_standard(data)
        ext = "xml"
    else:
        jdata = bin_to_standard(data)
        ext = "bin"

    if args.intermediate:
        write_data(data, f"{args.filename}.{ext}")

    if jdata:
        if args.normalize:
            jdata = normalize.normalize_common(jdata, lang)
        fh = open(f"{args.filename}.json", "w+")
        json.dump(jdata, fh, ensure_ascii=False, indent=2)
        logging.info("extracted %s to %s.json",
                     args.filename, args.filename)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
