import logging
import re

import json
import jsonschema

import os
import sys

from malie.lib import jsondiff

# Codes this app recognizes as being "localized" data
LANGCODES = {'en', 'es', 'fr', 'it', 'de', 'ptbr', 'jp'}

# This keymap is processed first, and has basic pattern matching.
# {lang} can be used to swap in that database's specific locale.
KEYMAP_RE = {
    r'^figs (.*)$':                       r"{lang} \1",  # Re-localization. Must happen before patterns below!
    r'ptb (.*)':                          r"ptbr \1",    # SM1.PTBR
}

# This KEYMAP is the next pass of processing. If there are ever
# two or more ways to spell the same field, they should be collapsed
# here.
KEYMAP = {
    'wt (kg)':   'weight (kg)',
    'mass (kg)': 'weight (kg)',
    '重さ(kg)':  'weight (kg)',
    'weight':    'weight (kg)',   # v1.1+ GUM

    'wt (lbs)':  'weight (lbs.)',
    'weight1':   'weight (lbs.)', # v1.1+ GUM

    'ht (m)':    'height (m)',
    '高さ(m)':   'height (m)',
    'height':    'height (m)',    # v1.1+ GUM

    'ht (ft)':   'height (ft.)',
    'height1':   'height (ft.)',  # v1.1+ GUM

    'pokémon #':            'national pokédex #',
    'national pokédex ':    'national pokédex #',
    '全国図鑑#':            'national pokédex #',
    'pokedex number':       'national pokédex #', # GUM.ES
    'national pokéesx #':   'national pokédex #', # SM35.ES
    'national pokédex':     'national pokédex #', # SM75.DE
    'national pokéitx #':   'national pokédex #', # SM35.IT

    'flavor text version':  'flavor version',
    'フレーバーバージョン': 'flavor version',

    'resistance amount':    'resistance value',
    'resistencance amount': 'resistance value', # GUM.ES (v1.1)
    '抵抗力数値':           'resistance value',

    'weakness amount':      'weakness value',
    '弱点数値':             'weakness value',

    'collection ':          'collection #',
    'regulations symbol':   'regulation symbol',
    'ダメージ':             'damage',
    'イラストレーター':     'illustrator',
    'ultra beast?1':        'ultra beast?', # Duplicated in SM7.FR

    'en flavor text (roman alphabet)': 'en flavor text',
    'en flavor (roman alphabet)':      'en flavor text',
    'en flavor (roman alpghabet)':     'en flavor text',  # Typo in SM6
    'en flavor(roman alphabet)':       'en flavor text',  # Typo in SM3.FR
    'en flavor':                       'en flavor text',  # SM8.DE
    'deflavor text':                   'de flavor text',  # SM8.DE

    'en attack text1':                 'en attack draft', # SM6.EN

    'en attack text edited':           'en attack text',  # SM3.ES; (Does not appear to be an edited/draft field.)
    'de attack name1':                 'de attack text',  # SM5.DE (v1.0) yes, 'name1' is misnamed 'text'
    'es attack name1':                 'es attack text',  # SM5.ES (v1.0)
    'fr attack name1':                 'fr attack text',  # GUM.FR (v1.1)
    'it attack name1':                 'it attack text',  # GUM.IT (v1.1)
    'ptbr attack name1':               'ptbr attack text', # GUM.PTBR (v1.1)
    'de attack text ':                 'de attack text',  # SM8.DE (v1.0)
    'ptbrattack text':                 'ptbr attack text', # SM8.PTBR (v1.0)
    'it attack text_marco':            'it attack text',  # SM7.IT (v1.0)

    'ptbrattack name':                 'ptbr attack name', # SM8.PTBR (v1.0)

    'es card name1':                   'es evolves from', # SM5.ES; ??
    'it card name1':                   'it evolves from', # SM5.IT; ??
    'de evolvde from':                 'de evolves from', # SM6.DE; (es ==> de)
    'en evolvde from':                 'en evolves from', # SM6.DE (es ==> de)
    'it evolves itom':                 'it evolves from', # SM35.IT (fr ==> it)
    'en evolves itom':                 'en evolves from', # SM35.IT (fr ==> it)

    'en catergory':                    'en category',     # GUM.EN (v1.1)
    'decategory':                      'de category',     # SM8.DE
    'itcategory':                      'it category',     # SM8.IT

    'タイプ':             'jp type',
    'コスト':             'jp attack cost',
    'ワザ名':             'jp attack name',
    'ワザの説明':         'jp attack text',
    'カード名':           'jp card name',
    'フォーマット':       'jp format',
    'フレーバー（漢字）': 'jp flavor text (kanji)',
    '弱点タイプ':         'jp weakness type',
    '抵抗力タイプ':       'jp resistance type',
    '進化前':             'jp evolves from',
    '種類':               'jp category',
}

# Another pattern-processing pass; this happens after KEYMAP to catch
# all of the various misspellings that occur when we need to match
# against a specific column.
KEYMAP_RE2 = {
    r'(en|fr|it|de|es|ptbr|jp) category': r"\1 species", # Arbitrary remap
}

# These are very fiddly corrections and happen almost last;
# These target a specific database so we don't risk overcorrection.
TARGETED_KEYMAPS = {
    'SM6_PTBR': {
        '#ref!': 'collection #'
    }
}

# This is the last mapping before localization splitting: These values
# are actually global/EFIGS, prevent them from being separated into
# the "en" locale by removing their localization prefix.
DELOCALIZATION_MAP = {
    'en card #':          'TPCi card #',
    'en resistance type': 'resistance type',
    'en type':            'type',
    'en weakness type':   'weakness type',
    'en rarity':          'rarity',
}

comments_map = {
    'exo':             'Dylan "Exobyte" Mayo',
    'hvb':             'Hollie Beg',
    'hvb comments':    'Hollie Beg',
    'hollie comments': 'Hollie Beg',
    'jim':             'Jim Lin',
    'jim comments':    'Jim Lin',
    'kyl':             'Kyle Sucevich',
    'kyle comments':   'Kyle Sucevich',
    'ks comments':     'Kyle Sucevich',
    'brg comments':    'Ben Regal',
    'cr comment':      'CR',

    'read aloud comments':   'Read Aloud',
    'read aloud responses':  'Read Aloud Responses',
    'templating comments':   'Templating',
    'templating responses':  'Templating Responses',
    'templating resolution': 'Templating Responses',
    'translator response':   'Translator Responses',
    'tpci comment':          'TPCi',

    'editing':               'Editing',
    'comments from editing': 'Editing',
    'editing comments':      'Editing',

    'r&d comments':          'R&D',
    'combo':                 'Misc',
    '備考':                  'Misc',

    'distributor comment':   'Distributor',   # ES only?
    'distributor comments':  'Distributor',   # ES only?
    'tpci feedback':         'TPCi Response', # ES only?

    'tpci editor feedback':  'TPCi Response', # FR only?
    'tpci editors comments': 'TPCi Response', # FR only?
    'review comments':       'Distributor',   # FR distributor comments...?
    'sea fr ed comments':    'TPCi Response', # SM35.FR
    'fr.sea eds comments 2017-05-31': 'TPCi Response', # SM4.FR
    'tpci editor feedback ': 'TPCi Response', # SM5.FR
    'fr-editing feedback':   'TPCi Response', # SM6.FR
    'seattle review':        'TPCi Response', # SM7.FR
    'editors comments':      'Editing',       # XY11.FR; errata for "EFIGS"
    'tpci comments':         'TPCi Response', # XY12.FR

    'tpci de editing comments': 'TPCi Response', # XY10.DE

    "editor's comment":         'TPCi Response', # SM3.IT
    "editor's comments":        'TPCi Response', # SM35.IT
    'tpci editors feedback':    'TPCi Response', # SM5.IT
    "editors' comments":        'TPCi Response', # SM7.IT
    "editor comments":          'TPCi Response', # SM75.IT
    "it.editor's comments":     'Editing',       # XY9.IT

    'ptbr.editing comments':    'editing comments', # SM6.PTBR
}

comment_keys = {
    "editor's response",       # GUM.PTBR
    "distribuitor's comments", # SM35.PTBR
    'tpci editor reply',       # SM5.PTBR
    "editor's response1",      # SM7.PTBR
    "tpci's comments",         # SM9.PTBR
    "distributor's comments",  # SM9.PTBR
    'tcpi comment',            # XY9.PTBR
    'tpci comment2',           # XY9.PTBR
    'tpci comment3',           # XY9.PTBR
    'tpci comment4',           # XY9.PTBR
    'tpci comment5',           # XY9.PTBR
    'tpci comment6',           # XY9.PTBR
    'tpci comment7',           # XY9.PTBR
    'copag reply',             # XY9.PTBR
    'copag reply1',            # XY9.PTBR
    'copag reply2',            # XY9.PTBR
    'copag revision',          # XY9.PTBR
    'copag revision1',         # XY9.PTBR
    'copag revision2',         # XY9.PTBR
    'copag revision3',         # XY9.PTBR
    'copag revision4',         # XY9.PTBR
    'copag comment',           # XY9.PTBR
}

ability_map = {
    "{lang}.attack name":      "{lang}.name",
    "{lang}.attack text":      "{lang}.text",
    "{lang}.translation text": "{lang}.draft", # SM8, SM9
    "{lang}.attack draft":     "{lang}.draft", # SM6
    'jp.attack cost':          'jp.cost',
    'en.cost':                 'cost',
    'damage':                  'damage',
}

ability_map_2 = {
    "attacktype{n}": 'type',   # Added in 1.5 SM12, uses explicit [1-4].
}

JPTYPES = {
    '●':  '{C}', # Colorless
    '無': '{C}', # Colorless
    '炎': '{R}', # Fire
    '闘': '{F}', # Fighting
    '雷': '{L}', # Lightning
    '草': '{G}', # Grass
    '水': '{W}', # Water
    '超': '{P}', # Psychic
    '妖': '{Y}', # Fairy
    '悪': '{D}', # Darkness
    '鋼': '{M}', # Metal
    '竜': '{N}', # Dragon
    '0コスト': '{--}', # No-cost
}

# English 1.3+ style types
TYPES = {
    'C': '{C}', # Colorless
    'R': '{R}', # Fire
    'F': '{F}', # Fighting
    'L': '{L}', # Lightning
    'G': '{G}', # Grass
    'W': '{W}', # Water
    'P': '{P}', # Psychic
    'Y': '{Y}', # Fairy
    'D': '{D}', # Darkness
    'M': '{M}', # Metal
    'N': '{N}', # Dragon
}

JSCHEMA = {
    #pylint: disable=bad-whitespace,bad-continuation
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',

    # Localized top-level data:
    'patternProperties': {
        '^(en|es|fr|it|de|ptbr|jp)\.card name$': {
            'type': 'string'
        },
        '^(en|es|fr|it|de|ptbr|jp)\.evolves from$': {
            'type': 'string'
        },
        '^(en|es|fr|it|de|ptbr|jp)\.species$': {
            'type': 'string'
        },
        '^(en|es|fr|it|de|ptbr|jp)\.flavor text$': {
            'type': 'string'
        },
        '^(jp)\.source$': {
            'type': 'string'
        },
        '^(jp)\.flavor text \(hiragana\)$': {
            'type': 'string'
        },
        '^(jp)\.flavor text \(kanji\)$': {
            'type': 'string'
        }
    },

    'properties': {
        'archetypeid':          { 'type': 'string' },
        'cardid':               { 'type': 'string' },
        'collection #':         { 'type': 'string' },
        'TPCi card #':          { 'type': 'string' },
	'rarity':               { 'type': 'string' },
    	'copyright':            { 'type': 'string' },
    	'expansion symbol':     { 'type': 'string' },
        'illustrator':          { 'type': 'string' },
        'regulation symbol':    { 'type': 'string' },
        'category':             { 'type': 'string',
                                  'enum': ['Pokémon', 'Trainer', 'Energy'] },
        'subcategory':          { 'type': 'string', },
        'hp':                   { 'type': 'integer', 'minimum': 10, 'maximum': 500 }, # Raikou, Entei, Suicune
        'type':                 { 'type': 'string' },
        'baseevolution':        { 'type': 'string' }, # Added in v1.3 as evolutionary family ID
        'resistance type':      { 'type': 'string' },
        'resistance value':     { 'type': 'string' },
        'retreat':              { 'type': 'integer' },
        'weakness type':        { 'type': 'string' },
        'weakness value':       { 'type': 'string' },
        'flavor version':       { 'type': 'string' },
        'height (ft.)':         { 'type': 'string' },
        'height (m)':           { 'type': 'string' },
        'weight (kg)':          { 'type': 'string' },
        'weight (lbs.)':        { 'type': 'string' },
        'national pokédex #':   { 'type': 'string' },
        'lv.':                  { 'type': 'string' },
        'reference':            { 'type': 'string',
                                  'description': 'Translation reference (non-US)' },
        'tags': {
            'type': 'object',
            'properties': {
                'GX':          { 'type': 'boolean' },
                'EX':          { 'type': 'boolean' },
                'Prism Star':  { 'type': 'boolean' },
                'TAG':         { 'type': 'boolean' },
                'Promo':       { 'type': 'boolean' },
                'Ultra Beast': { 'type': 'boolean' },
            },
            'additionalProperties': False
        },

        'abilities': {
            'type': 'array',
            'items': {
                'type': 'object',
                'properties': {
                    'cost':   { 'type': 'string' },
                    'damage': { 'type': 'string' },
                    'type':   { 'type': 'string' },
                },
                'patternProperties': {
                    '^(en|es|fr|it|de|ptbr|jp)\.name$': {
                        'type': 'string'
                    },
                    '^(en|es|fr|it|de|ptbr|jp)\.text$': {
                        'type': 'string'
                    },
                    '^(en|es|fr|it|de|ptbr|jp)\.draft$': {
                        'type': 'string'
                    }
                },
                'additionalProperties': False
            },
            'minItems': 1,
            'maxItems': 4
        },
        'comments':             { 'type': 'object' },
    },
    'required': ['TPCi card #', 'category', 'subcategory'],
    'additionalProperties': False
}

f = open("schema.json", "w+")
json.dump(JSCHEMA, f, ensure_ascii=False, indent=2)
f.close()

def valuemap(value):
    if isinstance(value, str):
        return re.sub(r'^"(.*?)"$', lambda m: m[1], value)
    return value

class CardDexJSON:
    def __init__(self, data, lang='en'):
        self.lang = lang
        self.name = data['name']
        self.data = self.normalize(data)

    def normalize(self, data):
        return {
            'name': data['name'],
            'rows': list(map(self.rowmap, data['rows'])),
        }

    def localize_key(self, key):
        groups = "|".join(LANGCODES)
        pattern = f"^({groups}) "
        key = re.sub(pattern, lambda m: m[1] + '.', key)
        return key

    def rowmap(self, row):
        res = {}
        for k, v in row.items():
            # Filter empty rows
            if not v:
                continue
            # Lowercase all keys, map all values:
            k = k.lower()
            v = valuemap(v)
            for pattern, repl in KEYMAP_RE.items():
                k = re.sub(pattern, repl, k)
                k = k.format(lang=self.lang)
            k = KEYMAP.get(k, k)
            for pattern, repl in KEYMAP_RE2.items():
                k = re.sub(pattern, repl, k)
                k = k.format(lang=self.lang)
            k = TARGETED_KEYMAPS.get(self.name, {}).get(k, k)
            k = DELOCALIZATION_MAP.get(k, k)
            k = self.localize_key(k)
            self.safe_set_key(res, k, v, name='res')

        self.group_abilities(res)
        self.process_types(res)
        self.process_abilities(res)
        self.group_comments(res)
        self.process_format(res)

        # Delete archetypeid and cardid if they aren't set meaningfully
        if res.get('archetypeid', None) == '-1':
            del res['archetypeid']
        if res.get('cardid', None) == '-1':
            del res['cardid']

        jsonschema.validate(instance=res, schema=JSCHEMA)
        return res

    charcodes = {
        '0': 'Basic',
        '1': 'Stage 1',
        '2': 'Stage 2',
        'G': 'GX',
        'I': 'Item',
        'T': 'Pokémon Tool',
        'S': 'Supporter',
        'N': 'Special Energy',
        'R': 'Basic Energy',
        'A': 'Stadium',
        'P': 'Prism Star',
        'O': 'Promo',
        'U': 'TAG',
        'E': 'EX',
        'M': 'Mega',
        'B': 'BREAK',
        '+': 'Restored',
        # Ultra Beast... ?
    }

    def decompose_tags(self, tagstr):
        corrections = {
            # SMBSP 1.3.10 erroneous tags:
            '0GUTEAM': '0GU'
        }
        tagstr = corrections.get(tagstr, tagstr)
        if set(tagstr) <= set(self.charcodes.keys()):
            # 1.3+ style format codes
            tags = {self.charcodes[key] for key in tagstr}
        else:
            # 1.0-1.2 style codes
            tags = {tag.strip() for tag in tagstr.split('/')}
        return tags

    def process_format(self, row):
        mapping = {
            'Prism Star (Prism Star)': ['Prism Star'],
            'Energy':                  ['Basic Energy'],
            'Sp. Energy':              ['Special Energy'],
            'TAG TEAM':                ['TAG'],
            'BREAK Evolution':         ['BREAK'],
            'Tool':                    ['Pokémon Tool'],
            'Basic EX':                ['Basic', 'EX'],
            'Mega EX':                 ['Mega', 'EX'],
            'たね':                    ['Basic'],
            '1進化':                   ['Stage 1'],
            '2進化':                   ['Stage 2'],
        }

        categories = {
            'Pokémon': {'Basic', 'Stage 1', 'Stage 2', 'Mega', 'Restored', 'BREAK'},
            'Trainer': {'Item', 'Pokémon Tool', 'Supporter', 'Stadium'},
            'Energy': {'Basic Energy', 'Special Energy'},
        }
        known_tags = {'EX', 'GX', 'TAG', 'Prism Star', 'Promo'}

        tags = self.decompose_tags(row.pop('en.format', ''))
        tags = tags | self.decompose_tags(row.pop('jp.format', ''))

        if not tags:
            if re.match(r"Basic {[A-Z]} Energy", row['en.card name']):
                tags.add('Basic Energy')
            elif self.lang == 'de' and row.get('collection #') == '086/SM-P':
                # Horrible, stupid workaround for a missing "EN Format" field;
                # This is Pikachu SM108.
                tags.add('Basic')
            elif 'Fossil' in row.get('en.evolves from'):
                # v1.4: No charcode for 'Restored' pokemon.
                tags.add('Restored')
            elif 'Old Amber' in row.get('en.evolves from'):
                # v1.4: No charcode for 'Restored' pokemon.
                tags.add('Restored')
            else:
                logging.warning("Cannot determine format of card!")
                return

        for tag in list(tags):
            for key, value in mapping.items():
                if tag.lower() == key.lower():
                    tags.remove(tag)
                    tags.update(value)

        queued = {}
        for tag in sorted(list(tags)):
            for category in categories:
                if tag in categories[category]:
                    assert row.get('category', category) == category
                    assert row.get('subcategory', tag) == tag
                    self.safe_set_key(row, 'category', category)
                    self.safe_set_key(row, 'subcategory', tag)
            if tag in known_tags:
                queued[tag] = True
                continue
            if not row.get('category'):
                queued[tag] = True
                logging.warning("unknown format tag %s", tag)

        tmp = row.pop('ultra beast?', None)
        if tmp == 'y':
            queued['Ultra Beast'] = True
        elif tmp:
            # smbsp 1.1 for IT/PTBR had some extra erroneous comments here:
            logging.warning("ignoring 'ultra beast?' field value %s", tmp)

        if queued:
            row['tags'] = queued

    @staticmethod
    def safe_set_key(entry, key, value, name='row'):
        if key in entry and entry[key] != value:
            logging.warning("Collision on %s[%s]", name, key)
            logging.warning("A) %s", entry[key])
            logging.warning("B) %s", value)
        entry[key] = value

    def group_abilities(self, row):
        # Search for Ability info and group them:
        # FIXME: This might look nicer if we use a regex to search for matching keys instead?
        for i in range(1, 5):
            ability = {}
            for base_key, base_dest_key in ability_map.items():
                base_key = base_key if i == 1 else f"{base_key} {i}"
                for lang in LANGCODES:
                    key = base_key.format(lang=lang)
                    dest_key = base_dest_key.format(lang=lang)

                    if key not in row:
                        continue

                    self.safe_set_key(ability, dest_key, row[key], 'ability')
                    del row[key]
            for base_key, dest_key in ability_map_2.items():
                key = base_key.format(n=i)
                if key not in row:
                    continue
                self.safe_set_key(ability, dest_key, row[key], 'ability')
                del row[key]

            if ability:
                row.setdefault('abilities', []).append(ability)

    @staticmethod
    def _normalize_typestr(typestr):
        for k, v in TYPES.items():
            typestr = typestr.replace(k, v)
        return typestr

    def process_abilities(self, row):
        # Post-processing of abilities
        for ability in row.get('abilities', []):
            # Delete draft text when it's redundant
            if ability.get('text', 'Foo') == ability.get('draft', 'Bar'):
                del ability['draft']

            if ability.get('cost'):
                ability['cost'] = self._normalize_typestr(ability['cost'])

            if ability.get('jp.cost'):
                tmp = ability['jp.cost']
                for k, v in JPTYPES.items():
                    tmp = tmp.replace(k, v)
                self.safe_set_key(ability, 'cost', tmp, 'ability')
                del ability['jp.cost']

    def process_types(self, row):
        for base_key in {'resistance type', 'type', 'weakness type'}:
            tmp = row.get(base_key, None)
            if tmp:
                for k, v in TYPES.items():
                    tmp = tmp.replace(k, v)
                row[base_key] = tmp

            key = f"jp.{base_key}"
            if key not in row:
                continue
            tmp = row[key]
            for k, v in JPTYPES.items():
                tmp = tmp.replace(k, v)
            self.safe_set_key(row, base_key, tmp)
            del row[key]

    def group_comments(self, row):
        # Search for Comments and group them:
        for key in comment_keys:
            comment = row.pop(key, None)
            if comment:
                self.safe_set_key(row.setdefault('comments', {}), key, comment, name='comments')

        for i in range(0, 2):
            for com_key in comments_map:
                key = com_key if i == 0 else f"{com_key}{i}"
                if key not in row:
                    continue

                name = comments_map[com_key]
                row.setdefault('comments', {}).setdefault(name, []).append(row[key])
                del row[key]

def normalize_common(data, lang='en'):
    return CardDexJSON(data, lang).data
