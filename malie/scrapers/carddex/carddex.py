#!/usr/bin/env python3

import argparse
import datetime
import json
import logging
import os
import re
import sys

import netfleece
import unitypack

import bindata
import normalize
import xmldata

from malie.lib import WebScraper
from malie.lib import cache

class ConfigProd:
    """Given a ConfigProd message, turn it into a rich object."""
    def __init__(self, filename):
        with open(filename, "r") as fh:
            json_obj = json.load(fh)
            self.file_name = json_obj['fileName']
            self.creation_date = json_obj['creationDate']
            self.ios_path = json_obj['data']['iphoneplayer_contentpath']
            self.android_path = json_obj['data']['android_contentpath']
            self.version = json_obj['data'].get('manifestVersion', 0)

class CardDexManifest:
    """CardDexManifest represents the asset manifest for the Card Dex app."""
    def __init__(self, json_obj, base=None):
        self.build = json_obj[0]
        self.stamp = json_obj[1]
        self.version = json_obj[2]
        self.assets = json_obj[3]
        self.base = base

    @staticmethod
    def from_json(infile, base=None):
        if isinstance(infile, str):
            with open(infile, "r") as fh:
                json_obj = json.load(fh)
        else:
            json_obj = json.load(infile)
        return CardDexManifest(json_obj, base)

    @staticmethod
    def from_unity(infile, base=None):
        jdata = CardDexManifest.unity_to_json(infile)
        return CardDexManifest(jdata, base)

    @staticmethod
    def unity_to_json(infile):
        fh = None
        if isinstance(infile, str):
            fh = open(infile, "rb")
        try:
            jdata = netfleece.parseloop(fh or infile, expand=True,
                                        backfill=True, crunch=True)
        finally:
            if fh:
                fh.close()
        return jdata

    def find_asset(self, name):
        for asset in self.assets:
            if asset['assetName'] == name:
                return asset
        raise Exception(f"can't find asset {name}")

    def asset_path(self, name):
        asset = self.find_asset(name)
        return f"{asset['s3Folder']}{asset['assetName']}"

    def asset_url(self, name):
        path = self.asset_path(name)
        return f"{self.base}{path}"

    def find_database(self, key, locale):
        res = []
        for asset in self.assets:
            match = re.match(f"db_{key}_([0-9]+_)?{locale}", asset['assetName'])
            if match:
                if len(match.groups()):
                    # HACK: sm3 matches sm3_5 erroneously; remove it.
                    # There's likely a better way to do this, but...?
                    if key == 'sm3' and match[1] == '5_':
                        continue
                res.append(asset)
        return res

    @staticmethod
    def asset_time(asset, version):
        prefix = f"{version}_"
        time = asset['s3Folder'].replace(prefix, '').rstrip('/')
        time = time.zfill(13)
        time_obj = datetime.datetime.strptime(time, '%Y%m%d_%H%M')
        return time_obj

    def find_latest_database(self, key, locale):
        candidates = self.find_database(key, locale)
        if not candidates:
            return None

        best = [candidates[0], self.asset_time(candidates[0], self.version)]
        now = datetime.datetime.now()
        for asset in candidates:
            time = self.asset_time(asset, self.version)
            logging.debug("fld; considering %s - %s - %s",
                          asset['assetName'],
                          asset['s3Folder'],
                          time.isoformat())
            if (now >= time) and (time > best[1]):
                best[0] = asset
                best[1] = time
                logging.debug("fld; best candidate is %s", str(best[0]))
        return best[0]

def get_release_dates(handle):
    ub = unitypack.load(handle)
    for key, obj in ub.assets[0].objects.items():
        if obj.type != 'TextAsset':
            continue
        data = obj.read()
        if data.name != 'releaseDateInfo':
            continue
        return json.loads(data.bytes)
    raise KeyError("TextAsset/releaseDateInfo not found in Unity file")

def get_set_keys(handle):
    ub = unitypack.load(handle)
    for key, obj in ub.assets[0].objects.items():
        if obj.type != 'CardDataManifest':
            continue
        data_obj = obj.read()

        # v1.0
        if 'setsToLoad' in data_obj:
            return data_obj['setsToLoad']

        # v1.2
        sets = []
        for series in data_obj['seriesToLoad']:
            logging.info("Found series %s", series['seriesID'])
            sets.extend(series['setsToLoad'])
        return sets
    raise KeyError("neither setsToLoad nor seriesToLoad objects found in Unity file")

def get_series_info(handle):
    ub = unitypack.load(handle)
    for key, obj in ub.assets[0].objects.items():
        if obj.type != 'SeriesInfo':
            continue
        data = obj.read()
        return [atom['cardDatabase'] for atom in data['databases']]
    raise KeyError("SeriesInfo object not found in Unity file")

# do either xml or bin; v1.0 uses xml, v1.2 uses bin, 1.3 uses a mixture
def extract_rawdb(handle):
    ub = unitypack.load(handle)
    for key, obj in ub.assets[0].objects.items():
        if obj.type == 'CardDataTable':
            return obj.read()['tableXML']
        if obj.type == 'TextAsset':
            # NB: data could still be text (XML) or binary!
            return obj.read().bytes
    raise KeyError("Neither TextAsset nor CardDataTable found in Unity file")

class CardDex:
    def __init__(self, version='1.0'):
        self.ws = WebScraper.WebScraper()
        self.cache = cache.Cache('carddex')
        self.version = version
        self.cpj = self._get_config()
        self.cdm = self._get_manifest()
        self._relinfo = None

    def _get_config(self):
        version = self.version.replace('.', '_')
        confname = f"ConfigProd_{version}.json"
        url = f"https://api.studio-prod.pokemon.com/ses1/Config/{self.version}/{confname}"
        self.cache.put(confname, fetcher=self.url_fetcher(url))
        return ConfigProd(self.cache.location(confname))

    def _grab(self, name, fetcher):
        path = os.path.join(self.version, name)
        ret = self.cache.fetch(path, fetcher=fetcher)
        logging.info("%s cached at %s",
                     name, self.cache.location(path))
        return ret

    def _get_manifest(self):
        def _fetcher():
            name = f"manifestBin_{self.version}"
            url = self.cpj.android_path + "manifestBin"
            mdata = self._grab(name, self.url_fetcher(url))
            mjson = CardDexManifest.unity_to_json(mdata)
            return json.dumps(mjson, indent=2).encode()

        name = f"manifestBin_{self.version}.json"
        data = self._grab(name, _fetcher)
        return CardDexManifest.from_json(data, self.cpj.android_path)

    def url_fetcher(self, url):
        return lambda: self.ws.fetch(url).content

    def keys(self):
        url = self.cdm.asset_url('carddatamanifest')
        with self._grab('carddatamanifest',
                        self.url_fetcher(url)) as data:
            return get_set_keys(data)

    def process_database(self, name, lang, args=None):
        name = name.lower()
        logging.debug("processing database %s", name)

        def _uni_fetcher():
            url = self.cdm.asset_url(name)
            handle = self._grab(name, self.url_fetcher(url))
            return handle

        def _uni_to_raw():
            with _uni_fetcher() as handle:
                data = extract_rawdb(handle)
                if isinstance(data, str):
                    data = data.encode()
                return data

        def _raw_fetcher():
            outname = f"{name}.raw"
            handle = self._grab(outname, _uni_to_raw)
            return handle

        def _raw_to_json():
            with _raw_fetcher() as handle:
                try:
                    jdata = bindata.parse_stream(handle)
                    jdata = bindata.standardize(jdata)
                except bindata.ParseError:
                    handle.seek(0)
                    jdata = xmldata.parse(handle)
                    jdata = xmldata.standardize(jdata)
                data = json.dumps(jdata, indent=2)
                return data.encode()

        def _json_fetcher():
            outname = f"{name}.json"
            handle = self._grab(outname, _raw_to_json)
            return handle

        def _normalize():
            with _json_fetcher() as handle:
                jdata = json.load(handle)
                jdata = normalize.normalize_common(jdata, lang)
                return json.dumps(jdata, indent=2).encode()

        if args and args.normalize:
            outname = f"{name}.cdjs"
            handle = self._grab(outname, _normalize)
        elif args and args.json:
            handle = _json_fetcher()
        elif args and args.raw:
            handle = _raw_fetcher()
        else:
            handle = _uni_fetcher()
        handle.close()

    def _release_date_info(self):
        # v1.1+
        url = self.cdm.asset_url('releasedateinfo')
        with self._grab('releasedateinfo',
                        self.url_fetcher(url)) as handle:
            return get_release_dates(handle)

    def _compute_releases(self):
        # v1.1+
        data = self._release_date_info()
        relinfo = {'released': {}, 'unreleased': {}}
        now = datetime.datetime.now(datetime.timezone.utc).replace(tzinfo=None)
        for release in data['releases']:
            rel = datetime.datetime.strptime(release['releaseDateUTC'],
                                             '%m/%d/%Y %H:%M:%S')
            key = 'released' if now >= rel else 'unreleased'
            for entry in release['cardDatabases']:
                lst = relinfo[key].setdefault(entry['seriesID'].lower(), [])
                lst.append(entry['databaseName'])
        return relinfo

    def _get_releases(self, key, lang):
        # v1.1+
        key = key.lower()
        if not self._relinfo:
            self._relinfo = self._compute_releases()
        return ["db_" + name.format(lang) for name in
                self._relinfo['released'].get(key, [])]

    def database_versions(self, key, lang="en", promiscuous=False):
        key = key.lower()

        # Any version:
        if promiscuous:
            return [asset['assetName'].lower() for
                    asset in self.cdm.find_database(key, lang)]

        # 1.0: get the names from carddata
        if self.version == '1.0':
            name = f"carddata_{key}_{lang}"
            url = self.cdm.asset_url(name)
            with self._grab(name, self.url_fetcher(url)) as data:
                db_names = get_series_info(data)
            return ["db_{}".format(name.lower()) for name in db_names]

        # 1.1+; get the names from the releaseDate manifest
        return self._get_releases(key, lang)


    def process_expansion(self, key, lang="en", args=None):
        key = key.lower()
        names = self.database_versions(key, lang)
        for name in names:
            try:
                self.process_database(name, lang, args)
            except WebScraper.Forbidden403:
                pass

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--raw', dest='raw', action='store_true')
    parser.add_argument('--json', dest='json', action='store_true')
    parser.add_argument('--normalize', dest='normalize', action='store_true')
    parser.add_argument('version', action='store', default='1.0', nargs='?')
    args = parser.parse_args()

    cdx = CardDex(args.version)
    for key in cdx.keys():
        for lang in ['en', 'es', 'fr', 'it', 'de', 'ptbr']:
            cdx.process_expansion(key, lang, args)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
