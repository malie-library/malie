#!/usr/bin/env python3
"""Metamon fetches PTCGO game data from the malie.io CDN."""

import json
import logging
import urllib.parse

import requests

from malie.lib import resource
# Bring the whole zoo
from malie.scrapers.ptcgo import safarizone
from malie.scrapers.ptcgo import logger

LOGGER = logger.get_logger()

DITTO = False
try:
    from malie.ditto import ditto
    DITTO = True
except ModuleNotFoundError:
    pass

class Metamon:
    def __init__(self):
        self.sfz = safarizone.get_safari_zone()
        self.base_url = 'https://malie.io/static/metamon/'
        self.localizations_file = 'unown.json'
        self.definitions_file = 'rotom.json'
        self.cache = self.sfz.cfg.cache.metamon

    @staticmethod
    def _get_release(locdata, url):
        req = requests.get(url)
        release = json.loads(req.content)
        locdata.load_release(release)

    def _update_locale(self, locale, data):
        LOGGER.info("Updating locale %s", locale)
        stored = self.sfz.localizations
        release_info: dict = stored.releases(locale)
        locdata = stored.get(locale)

        if locdata.version == data['version']:
            LOGGER.debug("I18N OK: %s.v%s", locale, locdata.version)
            return

        for release, reldata in data['releases'].items():
            checksum_remote = reldata['checksum']
            checksum_local = release_info.get(release)
            if checksum_remote != checksum_local:
                LOGGER.info("Updating %s:%s", locale, release)
                url = urllib.parse.urljoin(self.base_url, reldata['database'])
                self._get_release(locdata, url)
            else:
                LOGGER.debug("I18N OK: %s:%s:%s",
                             locale, release, checksum_local)
        locdata.version = data['version']

    def get_localizations(self):
        LOGGER.info("Updating localizations ...")
        url = urllib.parse.urljoin(self.base_url, self.localizations_file)
        req = requests.get(url)
        server_data = req.json()
        stored = self.sfz.localizations

        for locale, data in server_data.items():
            self._update_locale(locale, data)
        stored.save()

    @staticmethod
    def _get_deck(definitions, key, checksum, url):
        LOGGER.info("Fetching deck (%s:%s)", key, checksum, url)
        req = requests.get(url)
        deck = req.json()
        definitions.add_archetypes(key, checksum, deck)

    def get_definitions(self):
        LOGGER.info("Updating definitions ...")
        url = urllib.parse.urljoin(self.base_url, self.definitions_file)
        req = requests.get(url)
        server_data = json.loads(req.content)
        defs = self.sfz.definitions

        for key, entry in server_data.items():
            checksum = None
            try:
                checksum = defs.checksum(key)
            except KeyError:
                pass

            if entry['checksum'] != checksum:
                deck_url = urllib.parse.urljoin(self.base_url, entry['deck'])
                self._get_deck(defs, key, entry['checksum'], deck_url)
            LOGGER.info("deck OK: %s:%s", key, checksum)

        defs.save_decks()

    def _update_resource(self, name, data):
        filename = resource.get_resource(name, cache=self.cache)
        with open(filename, 'w') as outfile:
            json.dump(data, outfile, indent=2)

    def _get_resource(self, name):
        LOGGER.info("Fetching resource %s", name)
        url = urllib.parse.urljoin(self.base_url, name)
        req = requests.get(url)
        self._update_resource(name, req.json())

    def get_set_data(self):
        self._get_resource('SetDataMap.json')

    def get_family_map(self):
        self._get_resource('PokemonFamilyMap.json')

    def get_theme_decks(self):
        self._get_resource('ThemeDecks.json')

    def run(self):
        self.get_localizations()
        self.get_definitions()
        self.get_set_data()
        self.get_family_map()
        self.get_theme_decks()


def main():
    import argparse

    logger.standard_setup()

    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true",
                        help="Turn on debugging messages")
    args = parser.parse_args()

    if args.debug:
        logger.adjust_level(logging.DEBUG)

    LOGGER.info("Meta-meta?")
    client = Metamon()
    client.run()


def shim():
    if DITTO:
        LOGGER.info("Ditto!")
        ditto.main()
    else:
        main()


if __name__ == '__main__':
    shim()
