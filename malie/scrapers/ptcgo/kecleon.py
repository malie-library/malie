#!/usr/bin/env python3

import base64
from collections import namedtuple
import dataclasses
import gzip
import json
import locale
import logging
import os
import os.path
import re
import shutil
import subprocess
import tempfile
import urllib.parse
import zipfile

import editor
import requests
import xdg.BaseDirectory

from malie.scrapers.ptcgo import (
    growlithe,
    logger,
    manifest_history
)
from malie.scrapers.ptcgo.porygon import Manifest
from malie.lib.cache import Cache
from malie.lib.util import msnow
from malie.lib.resource import get_resource

LOGGER = logger.get_logger()


@dataclasses.dataclass
class URL:
    scheme: str
    netloc: str
    path: str
    params: str
    query: str
    fragment: str

    def __init__(self, url_str=''):
        self._parsed = urllib.parse.urlparse(url_str)
        self.scheme = self._parsed.scheme
        self.netloc = self._parsed.netloc
        self.path = self._parsed.path
        self.params = self._parsed.params
        self.query = self._parsed.query
        self.fragment = self._parsed.fragment

    def build(self):
        values = [str(v) for v in dataclasses.asdict(self).values()]
        return urllib.parse.urlunparse(values)

    def __str__(self):
        return self.build()


def de_cfg(data):
    items = data.decode().rstrip('\n').split('\n')
    return {k: v for k, v in [item.split('=', maxsplit=1) for item in items]}


def re_cfg(obj):
    items = []
    for k, v in obj.items():
        items.append("=".join((k, v)))
    strdata = "\n".join(items) + "\n"
    return strdata.encode()


class FetchError(Exception):
    """The web resource could not be acquired for whatever reason."""


def content_ok(rsp):
    """
    Functions that cache need to not cache an error response,
    so they can be wrapped with this to prevent that outcome.
    """
    if not rsp.ok:
        raise FetchError(rsp.reason)
    return rsp.content


def open_url_cached(url, cache, fetcher=None):
    """
    open_url_cached opens a URL, but looks in the cache first.
    the fetcher, if provided, is expected to accept a url string and
    return bytes.
    """
    def _lambda():
        if fetcher:
            return fetcher(url)
        return content_ok(requests.get(url=url))

    relpath = urllib.parse.urlparse(url).path.lstrip('/')
    return cache.fetch(relpath, fetcher=_lambda)


def fetch_url(url, headers=None, data=None):
    """Fetch a url with a very specific set of headers. Normally, the requests lib does some
    processing on the headers, but in this case we're trying to mimic the official client as
    closely as possible, so do a workaround to send a precise request."""
    session = requests.Session()
    if data:
        LOGGER.debug("POST data: %s", str(data))
        req = requests.Request('POST', url=url, headers=headers, data=data)
    else:
        req = requests.Request('GET', url=url, headers=headers)
    prp = req.prepare()
    rsp = session.send(prp)
    LOGGER.debug("%s %s", rsp.request.method, rsp.request.url)
    LOGGER.debug(json.dumps(dict(rsp.request.headers), indent=2))
    return rsp


Cachedirs = namedtuple('Cachedirs', ('base', 'data', 'manifests',
                                     'installation', 'deltas', 'patches',
                                     'manifest_diffs', 'bundles', 'metamon'))


class Kecleon:
    def __init__(self, cfgpath=None, cachepath=None, ignore=False):
        self.cfgpath = (cfgpath or
                        os.path.join(xdg.BaseDirectory.save_config_path("kecleon"),
                                     "config.json"))
        # Default Configuration / State:

        # Static State
        static = {
            'static_version': 4602,

            'url_config': '/clientconfig/config.json',
            'url_patch_manifest': '/patch/manifest.json',
            'url_motd': '/motd/motd.json',

            'assetDir': "bundle_link/{guid}/{platform}",
            'assetDirClassic': "bundles/{platform}",

            'manifestVersion': 'manifest.version',
            'manifestFile': "manifest_{manifest_version}.manifest",

            'platform': 'WindowsPlayer',
            'unityVersion': '2018.4.11f1',
            'unityVersionRefresher': '2018.4.11f1',
            'curlVersion': '7.52.0-DEV',
            'curlVersionRefresher': '7.52.0-DEV',

            'cakepath': 'Pokemon Trading Card Game Online_Data/cake.cfg',
            'dllpath': 'Pokemon Trading Card Game Online_Data/Managed/pie-src.dll',

            # cake.cfg might be unreliable, these are hardcoded backups:
            'hostname': 'tcgo-gateway.direwolfdigital.com',
            'versionURL': 'https://pie-live-dist.s3.amazonaws.com/',
            'assetURL': 'https://dfsqwbwcu8r1a.cloudfront.net/',
        }

        # cake.cfg: changes with each patch version. This is version 4602:
        cake = {
            'hostname': 'tcgo-gateway.direwolfdigital.com',
            'versionURL': 'https://pie-live-dist.s3.amazonaws.com/',
            'assetURL': 'https://dfsqwbwcu8r1a.cloudfront.net/',
            'IOSAppSecret': 'eebb3a53-dca7-4daa-9786-abbcab6200ac',
            'AndroidAppSecret': '261f05a9-f1ce-4278-a586-879f4a53bd60',
            'WindowsAppSecret': '141200fb-7c97-4a94-ab05-bb49d81f6fc8',
            'MacAppSecret': 'e533d4cf-4291-4e90-b2d4-3a669b2c1b3bversion=2.68.0.4602',
        }

        # Dynamic State
        dynamic = {
            'cakeVersion': '2.68.0.4602',
            'refresherVersionFull': '2.69.0.234',

            'cake': cake,
            'client': {},
            'patch_manifest': {},
            'motd': {},

            'patch': None,
            'status': None,
        }

        # Honest-to-goodness user configuration.
        config = {
            'cachepath': None,
            'datapath': None,
            'dragonite': {
                'b2-bucket': 'malie-io',
            },
            'manifestGzip': 'manifest_{manifest_version}.gz',
            'manifestCache': 'manifest_{manifest_version}.json',
        }

        # Config values are searched in this order:
        self._state = {
            'config': config,
            'dynamic': dynamic,
            'static': static,
        }

        if not ignore:
            self._load_kecleon_cfg()

        base = Cache("kecleon", cachepath or self.get('cachepath'))
        data = Cache("kecleon", self.get('datapath'), data=True)
        self.cache = Cachedirs(
            base=base,
            data=data,
            manifests=data.subcache('manifests'),
            installation=base.subcache('installation'),
            deltas=base.subcache('deltas'),
            patches=data.subcache('patches'),
            manifest_diffs=base.subcache('manifest_diffs'),
            bundles=data.subcache('bundles'),
            metamon=data.subcache('metamon'),
        )

        version_file = get_resource('versions.json', self.cache.metamon)
        self.vxs = manifest_history.SmartVersions(version_file, client=self)
        self.booted = False
        self._manifests = {}

    def __getitem__(self, key):
        for cfg in self._state.values():
            if key in cfg:
                return cfg[key]
        raise KeyError(f"Configuration key '{key}' not present")

    def __setitem__(self, key, value):
        for cfg in self._state.values():
            if key in cfg:
                cfg[key] = value
                return
        raise KeyError(f"Configuration key '{key}' not present; "
                       'store class is ambiguous')

    def _load_kecleon_cfg(self):
        try:
            with open(self.cfgpath, 'r') as f:
                LOGGER.info("Loaded configuration from %s", self.cfgpath)
                j = json.load(f)
                # User config always overrides default config:
                self._state.get('config', {}).update(j.get('config', {}))
                self._state.get('dynamic', {}).update(j.get('dynamic', {}))
                static = j.get('static', {})
                if static.get('static_version', 0) >= self['static_version']:
                    # File's version meets or exceeds Kecleon's default.
                    # Trust the user's version.
                    self._state.get('static', {}).update(static)
                else:
                    # Our version is bigger. Prefer our version.
                    static.update(self._state.get('static', {}))
                    self._state['static'] = static
        except FileNotFoundError:
            pass

    def get(self, key, default=None):
        for cfg in self._state.values():
            if key in cfg:
                return cfg[key]
        return default

    def save(self):
        LOGGER.info("Saving configuration to %s", self.cfgpath)
        os.makedirs(os.path.dirname(self.cfgpath), exist_ok=True)
        with open(self.cfgpath, 'w') as f:
            json.dump(self._state, f, indent=2)

    # Interpretations of specific data values:

    def cake_value(self, key, fallback_fn=None):
        """Retrieve a cached value from cake.cfg, falling back to a cached version if missing."""
        # cake.cfg might change out from under us, so here's a fallback.
        if key in self['cake']:
            return self['cake'][key]

        if fallback_fn:
            value = fallback_fn()
            if value:
                return value

        value = self[key]
        LOGGER.warning("cake.cfg did not set %s, falling back to hardcoded value %s", key, value)
        return value

    @property
    def content_version(self):
        vers = self['cakeVersion']
        return int(vers.split('.')[1])

    @property
    def build_version(self):
        vers = self['cakeVersion']
        return int(vers.split('.')[-1])

    @property
    def refresher_build(self):
        return int(self['refresherVersionFull'].split('.')[3])

    def asset_dir(self, guid, platform="pc"):
        if not guid:
            ret = self['assetDirClassic'].format(platform=platform)
        else:
            ret = self['assetDir'].format(guid=guid, platform=platform)
        return ret

    @property
    def version_url(self):
        return self.cake_value('versionURL')

    @property
    def asset_url_root(self):
        return self.cake_value('assetURL')

    def asset_url(self, filename, platform="pc", guid=None, version=False):
        """
        Returns a fully-formed asset URL.

        filename: The filename of the asset. Can include leading paths.
        platform: Optional, defaults to "pc".
        guid: The asset GUID to use. Defaults to Kecleon's latest known / current build GUID.
        version: bool; if true, use the versionURL based instead of the assetURL base.
        """
        return "{}/{}".format(self.asset_URL_base(platform, guid, version), filename)

    def bundle_url(self, filename, platform="pc", localestr="en_US", guid=None):
        """
        Returns a fully-formed bundle asset URL.

        filename: The filename for the bundle.
        locale: String, defaults to 'en_US'.
        guid: The asset GUID to use. Defaults to Kecleon's latest known / current build GUID.
        """
        return self.asset_url(f"{localestr}/{filename}", platform=platform, guid=guid)

    def repair_patch(self):
        manifest = self['patch_manifest']
        return manifest.get('WindowsClientRepairDownloadLink')

    # Settings/Properties that rely on the Version History:

    @property
    def guid(self):
        """Returns the latest GUID of the currently-emulated client version."""
        return self.vxs.guid(build=self.build_version)

    @property
    def manifest_version(self):
        return self.vxs.latest()

    def asset_URL_base(self, platform="pc", guid=None, version=False):
        """
        Returns the base of an asset URL, to which filenames could be appended.

        Hint: use asset_url for fully-formed URLs.

        platform: String, defaults to "pc".
        guid: String, defaults to Kecleon's latest known / current build GUID.
        version: Bool, defaults to false.
                 If true, uses the versionURL base instead of the assetURL base.

        return: String, in the form of http://example.com/path/to/folder, with no trailing slash.
        """
        root = self.version_url if version else self.asset_url_root
        url = "{root:s}{assetDir:s}"
        # NB: guid='' is valid, check against None explicitly.
        guid = self.guid if guid is None else guid
        return url.format(
            root=root,
            assetDir=self.asset_dir(guid, platform))

    def manifest_url(self, vers, guid=None):
        if guid is None:
            guid = self.vxs.guid(manifest=vers)
        filename = self['manifestFile'].format(manifest_version=vers)
        url = self.asset_url("{}?{}".format(filename, msnow()), guid=guid)
        return url

    # HTTP context-management functions:

    def _headers(self, url, post=False, cake=True):
        version = self['unityVersion'] if cake else self['unityVersionRefresher']
        cversion = self['curlVersion'] if cake else self['curlVersionRefresher']
        ua = "UnityPlayer/{} (UnityWebRequest/1.0, libcurl/{})".format(version, cversion)
        headers = {
            'Host':            urllib.parse.urlparse(url).netloc,
            'User-Agent':      ua,
            'Accept':          '*/*',
            'Accept-Encoding': 'identity'
        }
        if post:
            headers.update({
                'Proxy-Connection': 'Keep-Alive',
                'Content-Type':    'application/x-www-form-urlencoded',
                'X-Unity-Version': version,
                'Content-Length':  "",
            })
        else:
            headers['X-Unity-Version'] = version
        return headers

    @staticmethod
    def _youa_headers(url):
        auth = "{}:{}".format(
            'dwdpatcher',
            'The patches flow like sweet mead down the development spiral')
        return {
            'Content-Type': 'application/x-www-form-urlencoded',
            'User-Agent': 'Ye Olde User Agent',
            'accept-encoding': '*;q=1;gzip=0',
            'Authorization': "Basic {}".format(base64.b64encode(auth.encode())),
            'Connection': 'keep-alive',
            'Host': urllib.parse.urlparse(url).netloc
        }

    def _unity_fetch(self, url):
        return fetch_url(url=url, headers=self._headers(url))

    def _refresher_fetch(self, url):
        return fetch_url(url=url, headers=self._headers(url, cake=False))

    def _youa_fetch(self, url):
        return fetch_url(url=url, headers=self._youa_headers(url))

    # Manifest Discovery / Management #

    def fetch_manifest(self, version=None):
        version = version or self.manifest_version
        if version not in self._manifests:
            mani = Manifest(self.fetch_manifest_json(version))
            mani.version = version
            self._manifests[version] = mani
        return self._manifests[version]

    def fetch_manifest_json(self, vers=None):
        vers = vers or self.manifest_version
        cachefile = self['manifestCache'].format(manifest_version=vers)

        def _fetcher():
            # decompress gzip, return json data
            with self.fetch_manifest_gz(vers) as fh:
                gzfile = gzip.GzipFile(fileobj=fh)
                return gzfile.read()

        with self.cache.manifests.fetch(cachefile, fetcher=_fetcher) as jsonfh:
            return json.load(jsonfh)

    def fetch_manifest_gz(self, vers):
        url = self.manifest_url(vers)
        cachefile = self['manifestGzip'].format(manifest_version=vers)
        cache = self.cache.manifests
        return cache.fetch(cachefile,
                           fetcher=lambda: self._unity_fetch(url).content)

    # Patching Logic #

    def _execute_patch(self, patchfh):
        LOGGER.info("Executing patch")
        with zipfile.ZipFile(patchfh, 'r') as zfile:
            queues = {}
            install = self.cache.installation
            deltas = self.cache.deltas

            with zfile.open('PATCHDATA.M2H') as m2hfile:
                for line in m2hfile:
                    line = line.decode().strip()
                    act = line[0]
                    filename = line[1:]
                    LOGGER.info("[%s] %s", act, filename)
                    assert act in ('N', 'C', 'V', 'R')
                    queues.setdefault(act, []).append(filename)

            for filename in queues.get('C', []):
                install.mkdir(filename)
                LOGGER.info("Create %s", filename)

            for filename in queues.get('N', []):
                install.write(filename, zfile.read(filename))
                LOGGER.info("New %s", filename)

            for filename in queues.get('V', []):
                patchpath = deltas.write(filename, zfile.read(filename))
                # xdelta3 seems to be incapable of safely updating a file,
                # so we need to patch to a temp file and then overwrite it.
                tmpfd, tmpname = tempfile.mkstemp()
                os.close(tmpfd)
                path = install.location(filename)
                args = ['xdelta3', '-f', '-d', '-s', path, patchpath, tmpname]
                LOGGER.info(" ".join(args))
                res = subprocess.run(args)
                if res.returncode:
                    raise RuntimeError("xdelta3 could not patch {}; return code was {:d}".format(
                        filename, res.returncode))
                shutil.move(tmpname, path)

            for filename in queues.get('R', []):
                LOGGER.info("Remove %s", filename)
                try:
                    install.drop(filename, rmdir=True)
                except FileNotFoundError:
                    pass

    def _fetch_patch(self, url):
        auth = "{}:{}".format(
            'dwdpatcher',
            'The patches flow like sweet mead down the development spiral')
        headers = {
            'Authorization': "Basic {}".format(base64.b64encode(auth.encode())),
            'Accept-Encoding': 'gzip',
            'Host': '%s:443' % urllib.parse.urlparse(url).netloc,
        }

        def _fetch(patch_url):
            return content_ok(fetch_url(patch_url, headers=headers))

        return open_url_cached(url,
                               self.cache.patches,
                               fetcher=_fetch)

    def _cake_version_fallback(self):
        # v4602 shipped with a broken cake.cfg, try to salvage it:
        try:
            with self.cache.installation.open(self['cakepath']) as cakefh:
                data = cakefh.read().decode()
                matches = re.search(r"((?:\d+\.){3}\d+)", data)
                value = matches.group(1)
                LOGGER.warning("cake.cfg is damaged, but version was salvaged as %s", value)
                return value
        except:
            return None

    def inspect(self):
        """Inspect the donor installation and update dynamic configuration."""

        installation = self.cache.installation
        if not (installation.exists(self['cakepath']) and
                installation.exists(self['dllpath'])):
            self['status'] = 'corrupt'
            self.save()
            return
        self['status'] = 'ok'

        with installation.open(self['cakepath']) as cakefh:
            self['cake'] = de_cfg(cakefh.read())
        self['cakeVersion'] = self.cake_value('version', fallback_fn=self._cake_version_fallback)
        self.save()

        path = installation.location(self['dllpath'])
        guids = growlithe.main(path)
        if len(guids) != 1:
            LOGGER.warning("Could not extract GUID from %s", self['dllpath'])
            return

        LOGGER.info("found guid %s", guids[0])
        self.vxs.add_guid(self.build_version, guids[0])
        self.vxs.save()

    def _patch(self, url=None):
        """Given a patch URL, fetch and apply the patch."""
        url = url or self['patch']
        with self._fetch_patch(url) as patchfh:
            self._execute_patch(patchfh)
        self.inspect()

    def patch(self, url=None):
        """
        Given a patch URL, fetch and apply the patch.
        If the patch fails, mark the installation as corrupt.
        """
        try:
            self._patch(url)
            return
        except FetchError:
            raise
        except:
            self['status'] = 'corrupt'
            self.save()
            raise

    def update_refresher(self):
        LOGGER.info("Updating refresher")

        def _fetcher(refresher_url):
            return content_ok(self._youa_fetch(refresher_url))

        with open_url_cached(self['patch_manifest'].get('PatcherDownloadLink'),
                             self.cache.base,
                             fetcher=_fetcher) as f:
            zipf = zipfile.ZipFile(f, 'r')
            rawversion = zipf.read('version.txt')
            self.cache.base.write('version.txt', rawversion)
            self['refresherVersionFull'] = rawversion.decode().strip()
        self.save()

    # Status Check Logic #

    def fetch_config(self):
        LOGGER.info("Fetching client configuration")
        url = URL(self.version_url)
        url.path = self['url_config']
        url.query = msnow()
        rsp = self._unity_fetch(str(url))
        self['client'] = rsp.json()

    def fetch_patch_manifest(self, refresher=False):
        LOGGER.info("Fetching patch manifest")
        url = URL(self.asset_url_root)
        url.path = self['url_patch_manifest']
        if refresher:
            rsp = self._refresher_fetch(str(url))
        else:
            rsp = self._unity_fetch(str(url))
        self['patch_manifest'] = rsp.json()

    def check_manifest(self, guid=None):
        """Check the PTCGO server for the latest manifest version."""

        # Use the most recent GUID if one wasn't specified. (Resolve
        # the GUID here instead of down the stack so we can update the
        # version history.)
        guid = self.guid if guid is None else guid
        LOGGER.info("Checking for latest manifest (for %s guid; '%s')",
                    "latest" if self.guid == guid else "historical",
                    guid)
        url = URL(self.asset_url(self['manifestVersion'], guid=guid,
                                 version=True))
        url.query = msnow()

        rsp = self._unity_fetch(str(url))
        version = int(rsp.text)
        LOGGER.debug("Server's asset manifest version for guid='%s' is %d", guid, version)

        if self.vxs.add_manifest(guid, version):
            self.vxs.save()
            self.vxs.generate_diffs(self.cache.manifest_diffs)

        return version

    def fetch_motd(self):
        LOGGER.info("Fetching MOTD")
        url = URL(self.version_url)
        url.path = self['url_motd']
        url.query = msnow()
        rsp = self._unity_fetch(str(url))
        self['motd'] = rsp.json()

        loc, _enc = locale.getlocale()
        messages = self['motd'].get('message', {})
        if messages.get(loc, {}):
            msg = messages.get(loc)
        else:
            msg = messages.get('en_US')
        LOGGER.info("MOTD: %s", msg)

    def fetch_patchnotes(self):
        LOGGER.info("Fetching patch notes")
        url = URL(self['patch_manifest'].get('PatchNotesURL'))
        url.path = url.path.format('en_us')
        rsp = self._refresher_fetch(str(url))
        filename = self.cache.base.write('patchnotes.html', rsp.content)
        try:
            subprocess.run(['lynx', '-dump', filename])
        except FileNotFoundError:
            msg = "Can't display patchnotes: please install `lynx`"
            LOGGER.warning(msg)
            print(msg)

    def check_client(self):
        LOGGER.info("Checking for game client updates")
        loc_ver = self.build_version
        srv_ver = int(self['patch_manifest'].get('LatestClientVersion'))
        LOGGER.debug("Server version response: %s", srv_ver)
        self.vxs.add_build(srv_ver)

        diff = srv_ver - loc_ver
        if diff < 0:
            LOGGER.warning("Game version %d exceeds the server's %d; "
                           "Are we in the future?", loc_ver, srv_ver)
            return None

        if diff == 0:
            LOGGER.info("Server version %d is the latest, nothing to do.", loc_ver)
            return None

        LOGGER.info("Game version should be updated from %d to %d", loc_ver, srv_ver)
        if self['status'] == 'ok':
            patch = self['patch_manifest'].get('WindowsClientDownloadLink').format(loc_ver)
        else:
            patch = self.repair_patch()
        LOGGER.debug("Patch is expected at %s", patch)
        return patch

    def check_refresher(self):
        LOGGER.info("Checking refresher version")

        loc_ver = self.refresher_build
        srv_ver = int(self['patch_manifest'].get('LatestPatcherVersion'))

        diff = srv_ver - loc_ver
        if diff < 0:
            LOGGER.warning("Refresher version %d exceeds the server's %d; "
                           "Are we in the future?", loc_ver, srv_ver)
            return False

        if diff == 0:
            LOGGER.info("Refresher version %d is the latest, nothing to do.", loc_ver)
            return False

        logger.event("REFRESHER.NEW", "Found a new refresher build %d", srv_ver)
        LOGGER.info("Refresher version should be updated from %d to %d", loc_ver, srv_ver)
        return True

    # Update / Boot Logic #

    def refresh(self):
        """This logic follows the boot logic of the refresher.exe binary."""
        LOGGER.info("Refreshing game files")
        self.fetch_patch_manifest()
        self.fetch_patchnotes()
        patch = self.check_client()
        if not patch:
            return

        try:
            self.patch(patch)
        except FetchError:
            if patch == self.repair_patch():
                raise

        self.patch(self.repair_patch())

    def boot(self):
        """This logic follows the boot logic of the game client."""
        self.booted = False
        outdated = True
        while outdated:
            LOGGER.info("Booting ...")
            self.fetch_config()
            self.fetch_patch_manifest()
            outdated = self.check_refresher()
            if outdated:
                self.update_refresher()
            else:
                # NB: If the refresher updates, it will check the patcher.
                # This is for normal boots, from the game context.
                outdated = self.check_client()
            if outdated:
                self.refresh()
                LOGGER.info("Rebooting ...")
        self.check_manifest()
        self.fetch_motd()
        LOGGER.info("Done!")
        self.booted = True
        self.save()
        return self


def main():
    import argparse

    parser = argparse.ArgumentParser(description='Kecleon Client Manager')
    parser.add_argument('--edit', required=False, action='store_true',
                        help="Edit configuration file")
    parser.add_argument('--patch', dest='patch_url', required=False, action='store',
                        help="Explicitly install patch from URL")
    parser.add_argument('--install', required=False, action='store',
                        help="Install given build version. (e.g. 4602)")
    args = parser.parse_args()

    logger.standard_setup(level=logging.INFO)
    banner()
    LOGGER.info("Kecleon online!")
    kc = Kecleon()

    if args.edit:
        if not os.path.exists(kc.cfgpath):
            kc.save()
        editor.edit(kc.cfgpath)
        return

    if args.patch_url:
        kc.patch(args.patch_url)
        return

    if args.install:
        url = f"https://dn1bsy8uey7kh.cloudfront.net/StandaloneWindows_REPAIR_{args.install}.m2hpatch"
        kc.patch(url)
    else:
        kc.boot()


def banner():
    LOGGER.log(logging.DEBUG - 1, """
    ............................,...7?..............................................
    ...................,+,....,++..I7I,......:......................................
    ..................,++++..=++++IIIII.=7III=......................................
    .............,++,,=++++++++++:IIIIIIIIIII:......................................
    ..............++++++++++++++:III+=IIIIIII,......................................
    .............,I+++++++++++++7II:????~IIIIIIIII..................................
    ..............II+++++++++++=II????????:IIIIIII..................................
    ..............:I=+++++++++=III=????????,IIIII,..................................
    ..............,II:+++~+????III+++???????~IIIIIII7,..............................
    ...............III=~IIII?~777:+++++??????IIIIIII................................
    ..............,77.IIII?~77777III,?+++????,IIIII+................................
    .............,77II:I77777777I,,=III:+++??I?III=.................................
    .............77.I777777:,77IIIII?:II:+++??+IIIII,...............................
    ...........,77777777:III?7=IIIIII??II,++??:IIIIIII,.............................
    ..........,777777,IIIIII77~?II~~II?+II=++?,III+.................................
    .........,I7777~IIIIIIIII7I?II,7,??+~I~+++,,....................................
    .........?III7IIIIIIIIII,II++?,,,??+,I++++,,....................................
    ........,IIIIIIIIIIIIIII?II?++++++++,I,+++~.....................................
    .......,77II?IIIIIIIII????III+++++++II=+++?.....................................
    ......,:====~:~?IIIII,=???++III+,,?II~++++,.....................................
    .......,,,:,,:~+????I~=III:++,IIIII++++++,......................................
    ...........,?+++++++=:+??I=+I~++++++++++..:.....................................
    .............,:??+++++++++:=??I?+++++++..?I....,,.............,,,,,,,...........
    ................,:??++++++++?+++++++++,.~+?,:?I:..............,,,,,,,,,,........
    ....................,,+?+++++++++++++??,++++++?...............,,,,,,,,,,,,......
    .........................,+======~?????+~+++++,...............,,,:~:,,,,,,,.,,,,
    ........................III?++++,II?????+++++++?,..............,.~++=~:,,,,,,:.,
    .....................,III?~????:III??++++++++,.................,.:+++++=~:,,..,,
    ............,,,,,,,,III???:???III???++++++++,............,=IIII?~,=++++++=~:,,,.
    .....,,+????II???????????+~??III??+++~=++===,,........,????+++++++++?,+++++==~:,
    ....,,???????????=,,+IIIII?IIII??+++.++++=====,......?I?++++++++++++++?,+++++++=
    ....?.~7,,,..???I?IIIIII????????+++.===~~=======...,I??++++:+++++:++++++++++++++
    ..........,,I+,++??????????????+++++=======?,===,.,I?++++=+++++++++,+++++=++++++
    ...........,..:,..,:+?+++++++++++=,+======~++++~+::??++~++++++++++++:+++++:+++++
    ..........................+=~+~========:==~?++++++,++++?++++:+=,+++++~++++++++++
    ..........................=?+=========~++:~?:+++++?=++~++++=++++++++++:++++=++++
    ..........................,???+==?,==:+++~:?7I:I++++++=++++:+++++=++++~++++,++++
    ..........................,,??????++,++++?:I????~+++++++++++++++++++++~++++,++++
    ..........................,,??==????????+?+I????I+++:++=++++++++.+++++:++++~++++
    ..........................II,????????????~++,???++++~++++=++++,++++++:++++?+++++
    ..........................?II+????+++++++++++++++++?++++++++++++++++:+++++,+++++
    ..........................:IIII??+++++++++++++++++=+==++++++++++++,+++++++++++++
    ...........................IIIIIII=:?++++++?~:~:~+==++++::=++=::++++++++:+++++++
    ............................=IIIII?+++++,....=+=+++++++++++++++++++++++,==++++++
    .............................,III???++++,......:=++++++++++++++++++++~I7?~++++++
    ...............................,?????+++..........,??+++++++++++++~,=II7I==+++++
    ................................,???+++,...............,:+??+,,..,,,~III7+~+++++
    ................................I???++,..........................,,,:?7I7I~=++++
    .............................,I+,:,+,............................,,,,+7III+~++++
    ..............................?,.,I,..............................,,,+7II7I~=+++
    .................................,................................,.,=IIIII+~+++
    """)


if __name__ == '__main__':
    main()
