import datetime
import logging
import logging.handlers
import inspect
import os.path
import sys

from malie.lib.cache import Cache


def logdir():
    return Cache('malie/logs', data=True).basedir


def _get_name():
    stack = inspect.stack()
    # 0 is us, 1 is the get_logger call, and 2 is whoever called get_logger!
    frame = stack[2].frame
    name = frame.f_globals['__name__']

    if name == '__main__':
        filename = frame.f_globals['__file__']
        basename = os.path.basename(filename)
        name = os.path.splitext(basename)[0]

    return name


def get_logger(name=None):
    if not name:
        name = _get_name()

    logger = logging.getLogger(name)
    logger.addHandler(logging.NullHandler())

    return logger


LOGGER = get_logger()
EVENT_LOGGER = get_logger('EVENT')


def standard_setup(level=logging.INFO):
    sh = logging.StreamHandler(sys.stdout)
    if sys.platform.startswith('darwin'):
        path = '/var/run/syslog'
    else:
        path = '/dev/log'
    lsh = logging.handlers.SysLogHandler(address=path)
    lsh.ident = 'malie.io: '
    # No need for timestamp or logging level in the syslog.
    lsh.setFormatter(logging.Formatter("%(name)s:%(message)s"))

    sh.setLevel(level)
    sh.set_name("malie.stdout")
    logging.basicConfig(level=logging.DEBUG, handlers=[sh, lsh])

    event_logname = datetime.datetime.now().isoformat()
    logpath = os.path.join(logdir(), f"{event_logname}.events.log")
    event_handler = logging.FileHandler(logpath, delay=True)
    EVENT_LOGGER.addHandler(event_handler)


def adjust_level(level):
    root = logging.getLogger()
    for handler in root.handlers:
        if handler.name == "malie.stdout":
            handler.setLevel(level)
            return
    raise RuntimeError("Couldn't find the stdout handler, was it moved?")


def event(name, message, *args, **kwargs):
    msg = f"{name}: {message}"
    EVENT_LOGGER.info(msg, *args, **kwargs)
