#!/usr/bin/env python3

import argparse
import datetime
import os

from malie.scrapers.ptcgo import safarizone


def generate_log(outfile=None,
                 do_print=False):

    rt = safarizone.get_safari_zone().definitions
    datedcards = rt.cards.filter(lambda a: a.attr('dateAdded') is not None)
    datedcards = sorted(datedcards.values(),
                        key=lambda a: (a.attr('dateAdded'), a.sortkey()),
                        reverse=True)

    if outfile:
        f = open(outfile + ".tmp", "w")
    else:
        f = None

    for card in datedcards:
        time = card.attr('dateAdded') / 1000
        date = datetime.datetime.utcfromtimestamp(time)
        entry = "{} {}".format(date.strftime('%Y-%m-%d'), str(card))
        if f:
            f.write("{}\n".format(entry))
        if do_print:
            print(entry)

    if outfile:
        f.close()
        try:
            os.unlink(outfile)
        except FileNotFoundError:
            pass
        os.rename(outfile + ".tmp", outfile)


def main():
    parser = argparse.ArgumentParser(description='Cardlog Generator',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', dest='print', required=False, action='store_true',
                        help="Display card log to stdout")
    parser.add_argument('-o', dest='outputfile', required=False, action='store',
                        help="Write card log to file")
    args = parser.parse_args()

    generate_log(outfile=args.outputfile,
                 do_print=args.print)


if __name__ == '__main__':
    main()
