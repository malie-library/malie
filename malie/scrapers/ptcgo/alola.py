#!/usr/bin/env python3

import logging
import os
import shutil

from malie.scrapers.ptcgo import logger
from malie.lib import resource

LOGGER = logger.get_logger()


def check_external(tool, cfg):
    path = os.environ.get('PATH', os.defpath)
    if 'path' in cfg:
        path = path + os.pathsep + cfg['path']
    filepath = shutil.which(tool, path=path)
    if filepath:
        print(f"{tool: <8}:    {filepath}")
    else:
        print(f"{tool: <8}: NOT FOUND")
        print("\t", cfg['effect'])


def main():
    from malie.scrapers.ptcgo import safarizone

    logger.standard_setup(level=logging.INFO)
    LOGGER.info("Alola!")

    print("Malie core toolset v{}".format(resource.get_version()))
    git_mode = resource.git_mode()
    if git_mode:
        print("Running in editable/develop mode under git SCM control")
        print("Installed from version v{}".format(resource.install_version()))

    sfz = safarizone.get_safari_zone()

    external = {
        'lynx': {
            'effect': 'You will be unable to view patchnotes from kecleon.',
        },
        'convert': {
            'effect': 'You will be unable to crop cards with smeargle.',
        },
        'oxipng': {
            'path': os.path.expanduser('~/.cargo/bin'),
            'effect': 'You will be unable to crush png files with smeargle.',
        },
        'xdelta3': {
            'effect': 'You will be unable to patch to some new versions with kecleon.',
        },
    }

    print("\n")
    print("External dependencies:\n")
    for tool, cfg in external.items():
        check_external(tool, cfg)

    print("\n")
    print("Optional dependencies:\n")

    dragonite = None
    try:
        from malie.scrapers.ptcgo import dragonite
    except ModuleNotFoundError:
        pass

    if dragonite:
        print("Dragonite: OK!")
    else:
        print("Dragonite: Dependencies not found.")

    ditto = None
    try:
        from malie.ditto import ditto
    except ModuleNotFoundError:
        pass

    if ditto:
        print("Ditto:     OK!")
    else:
        print("Ditto:     Not found.")

    print("\n")
    print("Configuration:\n")
    print("Kecleon configuration:  {}".format(sfz.cfg.cfgpath))
    print("Kecleon cache:          {}".format(sfz.cfg.cache.base.basedir))
    print("  Installation:         {}".format(sfz.cfg.cache.installation.basedir))
    print("Kecleon data:           {}".format(sfz.cfg.cache.data.basedir))
    print("  Manifests:            {}".format(sfz.cfg.cache.manifests.basedir))
    print("  Patches:              {}".format(sfz.cfg.cache.patches.basedir))
    print("Manifest history:       {}".format(sfz.cfg.cache.manifest_diffs.basedir))
    print("Event logs:             {}".format(logger.logdir()))

    art = sfz.art_extractor
    print("Smeargle Bundle cache:  {}".format(art.cache.basedir))
    print("Smeargle art directory: {}".format(art.data.basedir))
    if dragonite:
        print("Dragonite cache:        {}".format(dragonite.cache().basedir))
    if not git_mode:
        print("Distribution files:     {}".format(resource.directory()))

    print("\n")
    print("Version Information:\n")
    print("Kecleon Mimicking:")
    print("  Refresher: {}".format(sfz.cfg['refresherVersionFull']))
    print("  Client:    {}".format(sfz.cfg['cakeVersion']))
    print("  GUID:      {}".format(sfz.cfg.guid))
    print("  Manifest:  {}".format(sfz.cfg.manifest_version))


if __name__ == '__main__':
    main()
