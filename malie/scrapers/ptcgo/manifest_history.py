#!/usr/bin/env python3

import enum
import io
import json
import logging
import requests

from malie.scrapers.ptcgo import (
    logger,
)

LOGGER = logger.get_logger()


class SearchMethod(enum.Enum):
    thorough = 1
    semithorough = 2
    standard = 3
    fast = 4
    lazy = 5


class Versions:
    def __init__(self, filename):
        self.fname = None
        self._vmap = {}
        self.load(filename)
        self.dirty = False

    @staticmethod
    def _normalize(vmap):
        vmap.setdefault('manifests', {})
        guids = vmap.setdefault('guids', {})
        intguids = {}
        for build, guid_list in guids.items():
            # json stringifies all keys. un-stringify the build versions.
            build = int(build)
            intguids[build] = guid_list
        vmap['guids'] = intguids

        return vmap

    def load(self, fname):
        with open(fname, "r") as infile:
            vmap = json.load(infile)
        self._vmap = self._normalize(vmap)
        self.fname = fname

    def save(self, fname=None):
        self.fname = fname or self.fname
        if not self.fname:
            msg = "No filename stored from init or load; cannot save (empty) version map"
            raise ValueError(msg)
        outmap = {'manifests': {}, 'guids': {}}

        order = []
        manifests = self._vmap['manifests']
        for guid, versions in manifests.items():
            maxv = max(versions)
            order.append((maxv, guid))

        # Sort GUIDs by highest-known correlated manifest version, high-to-low:
        order = reversed(sorted(order))
        for maxv, guid in order:
            outmap['manifests'][guid] = list(reversed(sorted(manifests[guid])))

        # Sort builds newest-to-oldest:
        guids = self._vmap['guids']
        builds = reversed(sorted(guids.keys()))
        for build in builds:
            # NB: json.dump will stringify build numbers on serialization.
            outmap['guids'][build] = guids[build]

        self._vmap = outmap
        with open(self.fname, 'w') as outfile:
            json.dump(outmap, outfile, indent=2)

    def flush(self, fname=None):
        if self.dirty:
            self.save(fname)
            self.dirty = False

    def add_guid(self, build, guid):
        build = int(build)
        guid_list = self._vmap['guids'].setdefault(build, [])
        if guid not in guid_list:
            self.dirty = True
            guid_list.append(guid)
            logger.event("GUID.NEW", "Discovered GUID %s for build %d",
                         guid, build)
            return True
        return False

    def add_manifest(self, guid, version):
        version = int(version)
        dirtied = False

        latest = self.latest()

        if version not in self.versions(guid):
            self.dirty = True
            dirtied = True
            self._vmap['manifests'].setdefault(guid, []).append(version)
            logger.event("MANIFEST.NEW", "Discovered manifest version %s for GUID %s",
                         version, guid)

        if version > latest:
            logger.event("MANIFEST.LATEST", "Manifest version %s is the most recent", version)

        return dirtied

    def add_build(self, build):
        build = int(build)
        dirtied = False
        if build not in self.builds():
            self._vmap['guids'][build] = []
            self.dirty = True
            dirtied = True
            logger.event("BUILD.NEW", "Discovered new build %d", build)
        return dirtied

    def guids(self, *, build=None, manifest=None):
        if (build is None and manifest is None) or (build and manifest):
            raise ValueError("Must specify either build or manifest argument, "
                             "but not both.")
        if build:
            return self._vmap['guids'][build]

        assert manifest is not None
        results = []
        for guid in self.all_guids():
            if manifest in self.versions(guid):
                results.append(guid)

        if not results:
            raise KeyError("Manifest version {:d} not found".format(manifest))
        return results

    def guid(self, *, build=None, manifest=None):
        guids = self.guids(build=build, manifest=manifest)
        return guids[-1]

    def builds(self):
        return self._vmap['guids'].keys()

    def all_guids(self):
        all_guids = []
        # Builds, newest to oldest
        for build in self.builds():
            # GUIDs, newest to oldest
            guids = reversed(self.guids(build=build))
            for guid in guids:
                if guid not in all_guids:
                    all_guids.append(guid)
        return all_guids

    def lower_bound(self, guid):
        versions = self.versions(guid)
        minv = min(versions)

        highest_max = 0
        for other_guid in self.all_guids():
            other_max = max(self.versions(other_guid))
            if minv > other_max > highest_max:
                highest_max = other_max

        return highest_max

    def upper_bound(self, guid):
        versions = self.versions(guid)
        maxv = max(versions)

        lowest_min = None
        for other_guid in self.all_guids():
            other_min = min(self.versions(other_guid))
            if other_min > maxv and ((lowest_min is None) or (other_min < lowest_min)):
                lowest_min = other_min

        return lowest_min or maxv

    def all_versions(self):
        result = set()
        for versions in self._vmap['manifests'].values():
            for version in versions:
                result.add(version)
        return sorted(list(result))

    def versions(self, guid):
        return self._vmap['manifests'].get(guid, [])

    def latest(self, guid=None):
        if guid:
            versions = self.versions(guid)
        else:
            versions = self.all_versions()
        return versions[-1] if versions else None


class SmartVersions(Versions):
    """Versions, extended to make some use of the Kecleon Client to discover new things."""
    def __init__(self, filename, client):
        self._client = client
        super().__init__(filename)

    def discover(self, method=SearchMethod.standard):
        lazy = False
        drought = 0
        guids = self.all_guids()

        for guid in guids:
            found_new = False
            skip = None
            LOGGER.info("Discovering manifests for guid %s", guid)
            self._client.check_manifest(guid)
            latest_version = max(self.all_versions())
            versions = self.versions(guid)

            if method == SearchMethod.thorough:
                # check from beginning of time to the very latest manifest
                minv = 0
                maxv = latest_version
            elif method == SearchMethod.semithorough:
                # lower_bound to global_latest
                minv = self.lower_bound(guid)
                maxv = latest_version
            elif method == SearchMethod.standard:
                # lower_bound to upper_bound
                minv = self.lower_bound(guid)
                maxv = self.upper_bound(guid)
            elif method == SearchMethod.fast:
                # lower_bound to upper_bound, but skip the middle,
                # likely previously explored region
                minv = self.lower_bound(guid)
                maxv = self.upper_bound(guid)
                skip = (min(versions), max(versions))
            elif method == SearchMethod.lazy:
                lazy = True
                minv = self.lower_bound(guid)
                maxv = self.upper_bound(guid)
                skip = (min(versions), max(versions))
            else:
                raise ValueError("Unknown SearchMethod")

            LOGGER.info("Checking for manifests between [%d, %d]", minv, maxv)
            if skip:
                LOGGER.info("(Skipping [%d, %d])", skip[0], skip[1])
            for i in range(minv, maxv + 1):
                if skip and skip[0] <= i <= skip[1]:
                    continue
                url = self._client.manifest_url(vers=i, guid=guid)
                rsp = requests.head(url)
                if rsp.status_code == 200:
                    LOGGER.info("Discovered version '%s'/%d", guid, i)
                    found_new = self.add_manifest(guid, i) or found_new

            if not found_new:
                drought += 1
                if lazy and drought >= 3:
                    LOGGER.info("Nothing new found for the latest three GUIDs, giving up")
                    return

    def generate_manifest_diff(self, diffcache, last_version, version):
        filename = f"manifest_{last_version}_{version}.txt"

        def _writer(outfile):
            # NB: Don't close this stream; it closes the outfile too,
            # and the cache wants to close that one.
            textstream = io.TextIOWrapper(outfile)
            mani_old = self._client.get_manifest(last_version)
            mani_new = self._client.get_manifest(version)
            print(f"# {last_version} --> {version}\n", file=textstream)
            mani_new.compare(mani_old, print_assets=True, file=textstream)
            textstream.detach()

        diffcache.put(filename, writer=_writer)

    def generate_diffs(self, diffcache):
        versions = self.all_versions()
        last_version = versions.pop(0)

        for version in versions:
            self.generate_manifest_diff(diffcache, last_version, version)
            last_version = version


def main():
    from malie.scrapers.ptcgo import safarizone
    from malie.lib.resource import get_resource

    logging.basicConfig(level=logging.INFO)

    client = safarizone.get_safari_zone().cfg
    filename = get_resource('versions.json',
                            client.cache.metamon)

    vxs = SmartVersions(filename, client)
    vxs.discover(SearchMethod.fast)
    vxs.flush()


if __name__ == '__main__':
    main()
