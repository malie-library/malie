#!/usr/bin/env python3

import json
import os

from malie.lib import jsondiff
from malie.scrapers.ptcgo import safarizone


def main():
    sfz = safarizone.get_safari_zone()
    cards = sfz.definitions.cards
    locale = 'en_US'
    loctable = sfz.localizations[locale]
    outdir = "exported"

    if not os.path.exists(outdir):
        os.makedirs(outdir, exist_ok=True)

    def localize(v):
        if isinstance(v, str):
            return loctable.detokenize(v, default='')
        return v

    for arch in cards.arch_keys:
        output = {}
        for card in sorted(cards.arch_key(arch).values()):
            output[card.guid] = card.attributes

        _ = jsondiff.rmap(localize, output)
        outfile = f"{locale}.{arch}.json"
        outpath = os.path.join(outdir, outfile)
        with open(outpath, "w") as f:
            json.dump(output, f, indent=2)

if __name__ == '__main__':
    main()
