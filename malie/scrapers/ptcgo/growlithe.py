#!/usr/bin/env python3
"""Growlithe can sniff the GUIDs out of pie-src.dll!"""

import os
import struct

from netfile import netfile
from malie.scrapers.ptcgo.stringtable import StringTable
from malie.scrapers.ptcgo import logger

LOGGER = logger.get_logger()

IGNORE_GUIDS = {'B9A4EA96-949E-11E1-890F-EFB676C7909C',
                'e079c0d3-b934-4fbd-b021-545106c75693',
                '71e99564-bfc7-47ed-9825-88faf4400a5c',
                'f88d4150-6fe8-4bea-beed-e7bb12024c7b',
                '5e5a3342-1a7d-422a-9564-6c0af365854f',
                '469cdc95-32a6-4553-b875-1a06e23c8dd3',
                '6a1dec5a-34db-4cee-a503-4ee759304135',
                '1414fd67-a632-4e38-ae04-0adf0074ac16',
                '98c83df9-ec82-4193-84a8-104115ce4e25',
                '6402e830-7fed-4cd1-b172-2a320047c2bb',
                '6b33d420-73cc-40d4-ada5-88a7d68063a9',
                '6a1dec5a-34db-4cee-a503-4ee759304136',
                '9f10a4a1-c4a7-4fed-b64f-1d52e9f288ec',
                '5b4cdd72-d1f6-4ccf-9d63-bd7e0ba4ce75',
                '9d42175a-0b7f-4059-863f-bcb42fe4aa3c',
                'b1f96fec-166b-421b-986d-c6ccba24680d',
                '99999999-9999-9999-9999-999999999999',
                '99999999-9999-9999-9999-999999999998',
                'a8465d46-6e95-49ea-aee0-dabd14a9d0f7',
                '00000000-0000-0000-0000-000000000001'}


def get_candidate(netb):
    # Begin hunting for PrivateImplementationDetails types/namespaces:
    candidates = []
    for typeobj in netb.typedefs:
        if "PrivateImplementationDetails" in typeobj.fullname:
            candidates.append(typeobj)

    # Look over all the fields for the candidate types we've identified,
    # and choose the largest one as a likely string table.
    best_candidate = None
    largest_size = 0
    largest_field = None
    for candidate in candidates:
        LOGGER.debug("Candidate: %s\n"
                     "flags: %s; extends: %s; field_list: %s; method_list: %s",
                     candidate.fullname,
                     candidate.flags, candidate.extends, candidate.field_list, candidate.method_list)
        for field in candidate.fields:
            # This parses the field signature:
            field.parse(netb)
            typeinfo = field.sig.type
            size = None
            # Is this a disgusting hack for determining the size of a field?
            # Yes.
            # Does it work?
            # ...Yes?
            if typeinfo.elemtype == netfile.ElementType.VALUETYPE:
                rodos = typeinfo.data
                if rodos.table == netfile.Table.TypeDef:
                    ptype = netb.typedefs[rodos.value - 1]
                    size = ptype.layout.class_size
            if size and size > largest_size:
                largest_size = size
                largest_field = field
                best_candidate = candidate

    return best_candidate, largest_field, largest_size


def method_body_addr(netb, field):
    # Grab the raw MethodDef table row entry for this method
    table_row = netb.tables[netfile.Table.MethodDef]['rows'][field - 1]
    # Parse out the RVA
    methodrva, = struct.unpack("<I", table_row[0:4])
    offset = netb.relative_to_absolute(methodrva)

    return offset


def get_idci4(data, index):
    # ldc.i4 opcode
    assert data[index] == 0x20
    return struct.unpack('<I', data[index + 1: index + 5])[0]


def add_string_table_indices(netb, candidate, strtab):
    for i in range(candidate.method_list, candidate.method_list_end):
        offset = method_body_addr(netb, i)
        netb.f.seek(offset, os.SEEK_SET)

        data = netb.f.read(1)
        # If the header isn't 0x92, this isn't what we want:
        if data != b'\x92':
            continue

        # Read in the method body
        size, = struct.unpack('<B', data)
        size = size >> 2
        data = netb.f.read(size)

        # Parse the string table indices out of the bytecode
        index = get_idci4(data, 5)
        assert index == get_idci4(data, 15)
        offset = get_idci4(data, 20)
        length = get_idci4(data, 25)
        strtab.add_index(index, offset, length)


def get_string_table(netb):
    candidate, field, size = get_candidate(netb)

    if not candidate:
        return StringTable(b'')

    # Read in the string table
    offset = netb.relative_to_absolute(field.rva.rva)
    netb.f.seek(offset, os.SEEK_SET)
    buff = netb.f.read(size)

    strtab = StringTable(buff)
    add_string_table_indices(netb, candidate, strtab)
    return strtab


def find_guids(strtable):
    # Find GUIDs in the heap, ignoring ones that don't change release-to-release.
    guid_pattern = r'[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}'
    guids = [match.group() for match in strtable.search(guid_pattern)]
    candidates = list(set(guids) - IGNORE_GUIDS)
    return candidates


def main(filename):
    pie = netfile.NETFile(filename)
    pie.parse()
    strtab = get_string_table(pie)
    guids = find_guids(strtab)
    return guids


def entry():
    import sys
    print(main(sys.argv[1]))


if __name__ == '__main__':
    entry()
