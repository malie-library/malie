types = {
    "root": {
        1: ("string", "classname"),
        2: ("varint", "classtype"),
        101: ("dwd.Protobuf.cake.item.AllAvatarArchetypesFound"),
        103: ("dwd.Protobuf.Progression.AllScenarios"),
        107: ("dwd.Protobuf.Collection.ArchetypesFound"),
    },

    "dwd.Protobuf.cake.item.AllAvatarArchetypesFound": {
        1: ("dwd.Protobuf.Archetype"),
        2: ("string", "checksum")
    },

    "dwd.Protobuf.Progression.AllScenarios": {
        # 1: ???
        2: ("Scenario"),
        3: ("Scenario"),
        4: ("Scenario"),
    },

    "dwd.Protobuf.Collection.ArchetypesFound": {
        1: ("dwd.Protobuf.Archetype", "_archetypes"),
        2: ("string", "_checksum"),
        3: ("string", "_key"),
    },

    "dwd.Protobuf.Archetype": {
        1: ("dwd.Protobuf.UUID"),
        2: ("dwd.Protobuf.Attribute"),
    },

    # Possibly an extension of "Archetype" of some kind?
    "Scenario": {
        1: ("dwd.Protobuf.UUID"),
        2: ("dwd.Protobuf.Attribute"),
        3: ("dwd.Protobuf.UUID"),
        4: ("string"),
        5: ("string"),
    },

    "dwd.Protobuf.UUID": {
        1: ("64bit", "_lo"),
        2: ("64bit", "_hi"),
    },

    "dwd.Protobuf.Attribute": {
        1: ("varint", "_name"),
        2: ("dwd.Protobuf.Object", "_value"),
        3: ("dwd.Protobuf.Object", "_originalValue"),
        4: ("dwd.Protobuf.Object", "_modValue"),
    },

    "dwd.Protobuf.Object": {
        1: ("varint", "_objectType"),    # type is technically dwd.Protobuf.Object+Type
        2: ("dwd.Protobuf.Object", "_arrayValue"),
        3: ("dwd.Protobuf.KeyObjectPair", "_dictionaryValue"),
        4: ("string", "_stringValue"),
        5: ("varint", "_boolValue"),       # It's a bool, but on the wire as varint.
        6: ("varint", "_intValue"),
        7: ("float", "_floatVaue"),
        8: ("dwd.Protobuf.UUID", "_guidValue"),
    },

    "dwd.Protobuf.KeyObjectPair": {
        1: ("string", "key"),                    # Guessing on the name
        2: ("dwd.Protobuf.Object", "value"),     # Guessing on the name
    },
}
