#!/usr/bin/python3
import json
import os

from malie.lib.DLO import DLO

def reldate(arg):
    '''Returns the earliest release date of a TPCI multiset.'''
    _, expansion = arg
    mindate = None
    for locdata in expansion.get('locales', {}).values():
        locdate = locdata.get('releaseDate', None)
        if locdate and (not mindate or locdate < mindate):
            mindate = locdate
    return mindate or "1970-01-01"

class LocalizedExpansion:
    def __init__(self, locale, data):
        self._locale = locale
        # Copy out the global data
        self._commondata = data.copy()
        if 'locales' in self._commondata:
            del self._commondata['locales']
        # Copy out the localized data
        self._locdata = data.get('locales', {}).get(locale, {}).copy()
        # Combine them into a merged view
        self._data = self._commondata.copy()
        self._data.update(self._locdata)

    def __getitem__(self, key):
        return self._data[key]

    def localized(self):
        return bool(self._locdata)

    def keys(self):
        return self._data.keys()

class ExpansionDatabase:
    global_keys = ('id', 'tn-series-slug', 'cdb-id',
                   'series-slug', 'card-slug', 'outlinks')

    def __init__(self, fname=None):
        self.data = DLO()
        if fname:
            self.fname = fname
            self._load()

    def _load(self):
        try:
            f = open(self.fname, "r")
            self.data = json.load(f, object_hook=DLO)
        except FileNotFoundError:
            pass

    def update(self, expansion):
        updated = False

        # Target expansion
        xid = expansion.data['id']
        if xid not in self.data:
            self.data[xid] = DLO()
        target = self.data[xid]

        # Universal properties of expansion
        universal = {x: expansion.data[x] for x in self.global_keys
                     if x in expansion.data}
        updated |= target.rupdate(universal)

        # Locale-specific properties of expansion
        if 'locales' not in target:
            target['locales'] = DLO()
        lname = expansion.locale.name()
        if lname not in target['locales']:
            target['locales'][lname] = DLO()
        sub_target = target['locales'][lname]
        local = {x: expansion.data[x] for x in expansion.data.keys()
                 if x not in self.global_keys}
        updated |= sub_target.update(local)

        return updated

    def __getitem__(self, key):
        return self.data[key]

    def _reorder(self):
        dbs = sorted(self.data.items(), key=reldate, reverse=True)
        newdlo = DLO()
        for k, v in dbs:
            newdlo[k] = v
        self.data = newdlo

    def write(self):
        self._reorder()
        tmpname = self.fname + ".tmp"
        with open(tmpname, "w+") as out:
            json.dump(self.data, out, indent=2)
        try:
            os.unlink(self.fname)
        except FileNotFoundError:
            pass
        os.rename(tmpname, self.fname)

    def find_set_by_name(self, name, locales=None):
        results = []
        for multiset in self.data.values():
            for lname, ldata in multiset.get('locales', {}).items():
                if locales and lname not in locales:
                    continue
                # Note: 'title-cdb' is problematic because it's not always unique.
                # New sets that use the same cdb title might conflict instead of
                # realizing that they are a new set. C'est la vie.
                if name in {ldata.get('title'),
                            ldata.get('title-common'),
                            ldata.get('title-original'),
                            ldata.get('title-short')}:
                    results.append(multiset['id'])
                    # (Next Multiset)
                    break
        if len(results) == 1:
            return results[0]
        if results:
            raise Exception("Ambiguous search term; got more than one result: {}".format(str(results)))
        return None

    def find_set_by_cdbid(self, cdbid):
        for multiset in self.data.values():
            if multiset.get('cdb-id') == cdbid:
                return multiset['id']
        return None
